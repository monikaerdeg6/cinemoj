package rs.ftn.skmd.cinemoj.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.ui.adapter.generic.RecyclerViewAdapterBase;
import rs.ftn.skmd.cinemoj.ui.adapter.listener.ReservationSelectedListener;
import rs.ftn.skmd.cinemoj.ui.view.ReservationItemView;
import rs.ftn.skmd.cinemoj.ui.view.ReservationItemView_;
import rs.ftn.skmd.cinemoj.ui.view.generic.ViewWrapper;

@EBean
public class MyReservationsAdapter extends RecyclerViewAdapterBase<Reservation, ReservationItemView> {

    @RootContext
    Context context;

    private ReservationSelectedListener reservationSelectedListener;

    @Override
    public ReservationItemView onCreateItemView(ViewGroup parent, int viewType) {
        return ReservationItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ReservationItemView> viewHolder, int position) {
        ReservationItemView view = viewHolder.getView();
        final Reservation reservation = items.get(position);
        view.bind(reservation, reservationSelectedListener);
    }

    public void setList(List<Reservation> reservations) {
        this.items = reservations;
        notifyDataSetChanged();
    }

    public void setReservationSelectedListener(ReservationSelectedListener reservationSelectedListener) {
        this.reservationSelectedListener = reservationSelectedListener;
    }
}
