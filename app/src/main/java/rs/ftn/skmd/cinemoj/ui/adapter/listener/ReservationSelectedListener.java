package rs.ftn.skmd.cinemoj.ui.adapter.listener;

import rs.ftn.skmd.cinemoj.domain.Reservation;

public interface ReservationSelectedListener {

    void reservationSelected(Reservation reservation);

}
