package rs.ftn.skmd.cinemoj.usecase.model;

import rs.ftn.skmd.cinemoj.domain.User;

public class Session {

    private String token;

    private User user;

    public Session() {}

    public Session(User user) {
        this.user = user;
    }

    public Session(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
