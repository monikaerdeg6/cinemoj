package rs.ftn.skmd.cinemoj.ui.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.Locale;

import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Movie;

@EFragment(R.layout.movie_info_fragment)
public class MovieInfoFragment extends Fragment {

    @ViewById
    ImageView moviePoster;

    @ViewById
    TextView movieTitle;

    @ViewById
    TextView movieDuration;

    @ViewById
    TextView movieGenre;

    @ViewById
    TextView movieYear;

    @ViewById
    RatingBar movieRating;

    @ViewById
    TextView movieDescription;

    public void bind(Movie movie, Context context) {
        movieTitle.setText(movie.getName());
        movieDuration.setText(String.format(Locale.getDefault(),
                "%s: %d", context.getString(R.string.duration), movie.getDuration()));
        movieGenre.setText(movie.getGenres());
        movieYear.setText(String.format(Locale.getDefault(),
                "%s: %d", context.getString(R.string.year), movie.getYear()));
        movieRating.setRating(movie.getRating());
        movieDescription.setText(movie.getDescription());
        loadPosterImage(movie.getPictureUrl());
    }

    @UiThread
    public void loadPosterImage(String url) {

        Glide.with(this)
                .asBitmap()
                .load(url)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        moviePoster.setImageBitmap(resource);
                    }
                });
    }
}
