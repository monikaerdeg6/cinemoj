package rs.ftn.skmd.cinemoj.repository.local.database.dao.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieDb;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieDb.MOVIE_TABLE_NAME;
import static rs.ftn.skmd.cinemoj.repository.local.database.entity.ScreeningDb.SCREENING_TABLE_NAME;

@Dao
public interface MovieDao extends BaseDao<MovieDb> {

    @Transaction
    @Query("SELECT * FROM " + MOVIE_TABLE_NAME + " WHERE movieId = :id LIMIT 1")
    Flowable<MovieDb> getById(long id);

    @Transaction
    @Query("SELECT * FROM " + MOVIE_TABLE_NAME)
    Flowable<List<MovieDb>> getAll();

    @Transaction
    @Query("DELETE FROM " + MOVIE_TABLE_NAME)
    void deleteAll();

    @Transaction
    @Query("DELETE FROM " + MOVIE_TABLE_NAME + " WHERE movieId = :id")
    void deleteById(long id);

    @Transaction
    @Query("SELECT * FROM " + MOVIE_TABLE_NAME +" as m LEFT JOIN  " + SCREENING_TABLE_NAME
            + " as s ON m.movieId=s.movieId WHERE s.movieId IS NULL")
    Flowable<List<MovieDb>> getSoon();

}