package rs.ftn.skmd.cinemoj.domain;

public enum Language {
    ENGLISH,
    SERBIAN
}
