package rs.ftn.skmd.cinemoj.repository.remote.dto;

public class MovieFeedbackDTO {

    private Long user_id;
    private Long reservation_id;
    private SoonMovieDetailsDTO movie;
    private Integer rate;
    private String text;
    private String hall;
    private String date;
    private String seats;

    public MovieFeedbackDTO() {
    }

    public MovieFeedbackDTO(Long user_id, Long reservation_id, Integer rate, String text) {
        this.user_id = user_id;
        this.reservation_id = reservation_id;
        this.rate = rate;
        this.text = text;
    }

    public MovieFeedbackDTO(Long user_id, Long reservation_id, SoonMovieDetailsDTO movie, Integer rate,
                            String text, String hall, String date, String seats) {
        this.user_id = user_id;
        this.reservation_id = reservation_id;
        this.movie = movie;
        this.rate = rate;
        this.text = text;
        this.hall = hall;
        this.date = date;
        this.seats = seats;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(Long reservation_id) {
        this.reservation_id = reservation_id;
    }

    public SoonMovieDetailsDTO getMovie() {
        return movie;
    }

    public void setMovie(SoonMovieDetailsDTO movie) {
        this.movie = movie;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

}
