package rs.ftn.skmd.cinemoj.repository.remote.error_extractor;

import retrofit2.Response;
import rs.ftn.skmd.cinemoj.usecase.model.Error;

public interface ErrorExtractor {
    Error extractErrorFromResponse(Response response);
}
