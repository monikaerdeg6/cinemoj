package rs.ftn.skmd.cinemoj.ui.activity;

import android.annotation.SuppressLint;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;

import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Location;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.ui.activity.map.LocationRequestProvider;
import rs.ftn.skmd.cinemoj.ui.activity.map.PermissionHandler;

import static rs.ftn.skmd.cinemoj.ui.activity.map.PermissionHandler.PERMISSIONS_ACCESS_LOCATION;

@EActivity(R.layout.activity_where_to_find_us)
public class WhereToFindUsActivity extends BaseActivity implements OnMapReadyCallback {

    private static final int REQUEST_CHECK_SETTINGS = 98;

    private GoogleMap googleMap;

    private FusedLocationProviderClient fusedLocationProviderClient;

    private Location currentLocation;

    private LatLng cinemojLocation =  new LatLng(45.253429, 19.84339);

    @Bean
    LocationRequestProvider locationRequestProvider;

    @Bean
    PermissionHandler permissionHandler;

    @FragmentById(R.id.map)
    SupportMapFragment mapFragment;

    @App
    CinemojApplication cinemojApplication;

    @AfterViews
    void initMap() {
        cinemojApplication.getDiComponent().inject(this);
        displayDrawer();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        LatLng address = cinemojLocation;
        this.googleMap.addMarker(new MarkerOptions().position(address).title("Cinemoj"));
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(address));
        this.googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 0.5 seconds.
        this.googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 500, null);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        if (permissionHandler.locationIsNotEnabled(this)) {
            permissionHandler.requestLocationPermission(this);
            return;
        }

        askTheUserToTurnOnLocation();
    }

    @SuppressLint("MissingPermission")
    private void startLocationScan() {
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                currentLocation = new Location(locationResult.getLastLocation().getLatitude(),
                        locationResult.getLastLocation().getLongitude());
                fusedLocationProviderClient.removeLocationUpdates(this);
                LatLng address = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                googleMap.addMarker(new MarkerOptions().position(address).title("Me"));
            }
        };

        fusedLocationProviderClient.requestLocationUpdates(locationRequestProvider.createLocationRequest(), locationCallback, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_ACCESS_LOCATION) {
            if (grantResults.length > 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                askTheUserToTurnOnLocation();
            } else {
                Toast.makeText(this, getString(R.string.location_not_enabled), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void askTheUserToTurnOnLocation() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequestProvider.createLocationRequest());

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, locationSettingsResponse -> {
            startLocationScan();

        });

        task.addOnFailureListener(this, exception -> {

            try {
                ResolvableApiException resolvable = (ResolvableApiException) exception;
                resolvable.startResolutionForResult(WhereToFindUsActivity.this,
                        REQUEST_CHECK_SETTINGS);
            } catch (IntentSender.SendIntentException sendEx) {
                sendEx.printStackTrace();
            }
        });
    }
}
