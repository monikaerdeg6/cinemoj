package rs.ftn.skmd.cinemoj.repository.remote.dao;

import java.net.HttpURLConnection;

import rs.ftn.skmd.cinemoj.repository.remote.error_extractor.ErrorExtractor;
import rs.ftn.skmd.cinemoj.usecase.model.Result;


public abstract class BaseRemoteDaoImpl {

    protected final ErrorExtractor errorExtractor;

    BaseRemoteDaoImpl(ErrorExtractor errorExtractor) {
        this.errorExtractor = errorExtractor;
    }

    Result convertResponseToResult(retrofit2.Response response) {
        if (response.code() != HttpURLConnection.HTTP_OK
                && response.code() != HttpURLConnection.HTTP_CREATED
                && response.code() != HttpURLConnection.HTTP_NO_CONTENT) {
            return new Result(errorExtractor.extractErrorFromResponse(response));
        }

        return new Result();
    }

}
