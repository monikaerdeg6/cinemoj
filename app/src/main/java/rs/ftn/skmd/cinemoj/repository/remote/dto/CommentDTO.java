package rs.ftn.skmd.cinemoj.repository.remote.dto;

import java.io.Serializable;

public class CommentDTO implements Serializable {

    private Long id;

    private String text;

    private String username;

    private Integer rate;

    public CommentDTO() {
    }

    public CommentDTO(Long id, String text, String username, Integer rate) {
        this.id = id;
        this.text = text;
        this.username = username;
        this.rate = rate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }
}
