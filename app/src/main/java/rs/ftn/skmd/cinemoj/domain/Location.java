package rs.ftn.skmd.cinemoj.domain;

import java.io.Serializable;

public class Location implements Serializable {

    private double longitude;
    private double latitude;

    public Location() {
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }
}
