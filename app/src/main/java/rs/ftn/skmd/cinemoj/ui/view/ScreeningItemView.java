package rs.ftn.skmd.cinemoj.ui.view;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.ui.adapter.listener.ScreeningSelectedListener;

@EViewGroup(R.layout.view_item_screening)
public class ScreeningItemView extends RelativeLayout {

    @ViewById
    TextView screeningName;

    @ViewById
    TextView screeningDetails;

    public ScreeningItemView(Context context) {
        super(context);
    }

    public void bind(Screening screening, ScreeningSelectedListener screeningSelectedListener) {
        screeningName.setText(screening.getMovie().getName());
        screeningDetails.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(screening.getDate()));

        this.setOnClickListener(v -> {
            if (screeningSelectedListener != null) {
                screeningSelectedListener.screeningSelected(screening);
            }
        });
    }
}


