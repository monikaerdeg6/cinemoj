package rs.ftn.skmd.cinemoj.system;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.usecase.dependency.system.FirebaseRegistrationTokenProvider;

@Module
public class SystemModule {

    private final Context context;

    public SystemModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    NetworkManager providesNetworkManager() {
        return new NetworkManagerImpl(context);
    }

    @Provides
    @Singleton
    NotificationChannelsManager provideNotificationChannelsManager() {
        return new NotificationChannelsManager(context);
    }

    @Provides
    @Singleton
    FirebaseRegistrationTokenProvider provideFirebaseTokenProvider() {
        return new FirebaseRegistrationTokenProviderImpl();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
