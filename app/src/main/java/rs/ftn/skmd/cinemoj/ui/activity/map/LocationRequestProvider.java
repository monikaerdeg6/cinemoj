package rs.ftn.skmd.cinemoj.ui.activity.map;

import com.google.android.gms.location.LocationRequest;

import org.androidannotations.annotations.EBean;

@EBean
public class LocationRequestProvider {

    private static final int INTERVAL_MS = 3000;
    private static final int FASTEST_INTERVAL_MS = 1000;

    public LocationRequest createLocationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(INTERVAL_MS);
        locationRequest.setFastestInterval(FASTEST_INTERVAL_MS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }
}
