package rs.ftn.skmd.cinemoj.usecase.dependency.repository.local;

public interface SecureStorage {

    void saveToken(String token);

    String getToken();

    void removeToken();

    void saveUserId(long userId);

    long getUserId();

    void removeUserId();

    void saveFirebaseRegistrationTokenToken(String firebaseRegistrationToken);

    String getFirebaseRegistrationTokenToken();

    void removeFirebaseRegistrationTokenToken();

    void clear();
}
