package rs.ftn.skmd.cinemoj.ui.activity;

import android.support.design.widget.NavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.ui.adapter.SoonAdapter;
import rs.ftn.skmd.cinemoj.usecase.MovieUseCase;
import rs.ftn.skmd.cinemoj.usecase.UserUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

@EActivity(R.layout.activity_soon)
public class SoonActivity extends BaseActivity {

    private static final String TAG = SoonActivity.class.getSimpleName();

    @Bean
    SoonAdapter soonAdapter;

    @ViewById
    RecyclerView recyclerView;

    @App
    CinemojApplication cinemojApplication;

    @Inject
    MovieUseCase movieUseCase;

    @Inject
    UserUseCase userUseCase;

    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);
        displayDrawer();
        initTitle();
        loadMovies();
    }

    private void initTitle() {
        if (userUseCase.getLoggedInUser() == null) return;
        if (userUseCase.getLanguage() == Language.ENGLISH) {
            setTitle("Soon");
        } else {
            setTitle("Uskoro");
        }
    }

    private void loadMovies() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false));
        recyclerView.setAdapter(soonAdapter);

        soonAdapter.setMovieSelectedListener(movie ->
                MovieDetailsActivity_.intent(this).movie(movie).start());

        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);
    }

    void loadData() {
        compositeDisposable.add(movieUseCase.getAllMovies()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleDataLoaded,
                        this::handleDataLoadFailed));
    }

    private void handleDataLoaded(ResultWithData<List<Movie>> movies) {
        soonAdapter.setList(movies.getValue());
    }

    private void handleDataLoadFailed(Throwable error) {
        Log.e(TAG, error.getMessage());
    }

}
