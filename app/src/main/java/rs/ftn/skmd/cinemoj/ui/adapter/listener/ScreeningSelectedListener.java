package rs.ftn.skmd.cinemoj.ui.adapter.listener;

import rs.ftn.skmd.cinemoj.domain.Screening;

public interface ScreeningSelectedListener {

    void screeningSelected(Screening screening);
}
