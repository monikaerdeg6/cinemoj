package rs.ftn.skmd.cinemoj.usecase.dependency.system;

import io.reactivex.Single;

public interface FirebaseRegistrationTokenProvider {

    Single<String> getToken();
}
