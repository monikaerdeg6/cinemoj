package rs.ftn.skmd.cinemoj.ui.activity;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentById;

import javax.inject.Inject;

import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.ui.fragment.MovieInfoFragment;
import rs.ftn.skmd.cinemoj.usecase.MovieUseCase;

@EActivity(R.layout.activity_movie_details)
public class MovieDetailsActivity extends BaseActivity {

    private static final String TAG = MovieDetailsActivity.class.getSimpleName();

    @Extra
    Movie movie;

    @FragmentById
    MovieInfoFragment movieInfoFragment;

    @App
    CinemojApplication cinemojApplication;

    @Inject
    MovieUseCase movieUseCase;

    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);
        displayDrawer();
    }

    @AfterViews
    void loadMovie() {
        movieInfoFragment.bind(movie, this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie_info_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (movie.isFavorite()) {
            item.setIcon(R.drawable.ic_favorite_border);
            showToast(getString(R.string.remove_from_favorites), Toast.LENGTH_SHORT);
        } else {
            item.setIcon(R.drawable.ic_favorite);
            showToast(getString(R.string.add_to_favorites), Toast.LENGTH_SHORT);
        }
        movieUseCase.toggleFavorite(movie.getId()).blockingAwait();
        return super.onOptionsItemSelected(item);
    }
}
