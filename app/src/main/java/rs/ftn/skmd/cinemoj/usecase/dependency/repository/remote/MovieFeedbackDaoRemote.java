package rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote;

import io.reactivex.Single;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public interface MovieFeedbackDaoRemote {

    Single<Result> save(MovieFeedback movieFeedback);

    //    Single<ResultWithData<List<MovieFeedback>>> getAllForMovie(Long movieId);

    Single<ResultWithData<MovieFeedback>> getFeedbackForReservation(Long reservationId);

}
