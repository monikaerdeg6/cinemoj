package rs.ftn.skmd.cinemoj.usecase;

import android.util.Log;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalScreeningDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.ScreeningRemoteDao;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class ScreeningUseCase {

    private static final String TAG = AuthenticationUseCase.class.getSimpleName();

    private final ScreeningRemoteDao screeningRemoteDao;

    private final LocalScreeningDao localScreeningDao;

    private final NetworkManager networkManager;

    public ScreeningUseCase(ScreeningRemoteDao screeningRemoteDao, LocalScreeningDao localScreeningDao,
                            NetworkManager networkManager) {
        this.screeningRemoteDao = screeningRemoteDao;
        this.localScreeningDao = localScreeningDao;
        this.networkManager = networkManager;
    }

    public Flowable<ResultWithData<List<Screening>>> getAll() {

        if (!networkManager.isNetworkAvailable()) {
            return localScreeningDao.getAll();
        }
        return screeningRemoteDao.getAll()
                .map(this::handleScreeningsResponse)
                .subscribeOn(Schedulers.io()).toFlowable();
    }

    public Single<Result> makeReservations(Long screeningId, Integer ticketsNumber) {

        return this.screeningRemoteDao.makeReservation(screeningId, ticketsNumber)
                .subscribeOn(Schedulers.io());
    }

    public Flowable<ResultWithData<List<MovieFeedback>>> getMovieComments(Long movie_id) {

        return screeningRemoteDao.getMovieComments(movie_id)
                .map(this::handleCommentsResponse)
                .subscribeOn(Schedulers.io()).toFlowable();
    }

    private ResultWithData<List<Screening>> handleScreeningsResponse(ResultWithData<List<Screening>>
                                                                             screeningsResult) {
        if (!screeningsResult.isSuccess()) {
            return new ResultWithData<>(screeningsResult.getError());
        }

        localScreeningDao.deleteAll().blockingAwait();
        localScreeningDao.saveAll(screeningsResult.getValue()).blockingAwait();

        return new ResultWithData<>(screeningsResult.getValue());
    }

    private ResultWithData<List<MovieFeedback>> handleCommentsResponse(ResultWithData<List<MovieFeedback>>
                                                                             commentsResult) {
        if (!commentsResult.isSuccess()) {
            return new ResultWithData<>(commentsResult.getError());
        }

        return new ResultWithData<>(commentsResult.getValue());
    }

    public List<Screening> filterScreenings(List<Screening> screenings, Date startDate, Date endDate) {
        return screenings.stream().filter(screening -> screening.getDate().after(startDate)
                && screening.getDate().before(endDate)).collect(Collectors.toList());
    }
}
