package rs.ftn.skmd.cinemoj.repository.remote.dto;

public class ReservationDTO {

    private Long screening_id;
    private Integer ticketsNumber;

    public ReservationDTO() {
    }

    public ReservationDTO(Long screening_id, Integer ticketsNumber) {
        this.screening_id = screening_id;
        this.ticketsNumber = ticketsNumber;
    }

    public Long getScreening_id() {
        return screening_id;
    }

    public void setScreening_id(Long screening_id) {
        this.screening_id = screening_id;
    }

    public Integer getTicketsNumber() {
        return ticketsNumber;
    }

    public void setTicketsNumber(Integer ticketsNumber) {
        this.ticketsNumber = ticketsNumber;
    }
}
