package rs.ftn.skmd.cinemoj.repository.remote.dao;

import org.modelmapper.ModelMapper;

import java.net.HttpURLConnection;

import io.reactivex.Single;
import retrofit2.Response;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.repository.remote.api.ApiService;
import rs.ftn.skmd.cinemoj.repository.remote.api.UnauthenticatedApiService;
import rs.ftn.skmd.cinemoj.repository.remote.dto.FirebaseTokenDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.UserCredentialsDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.UserDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.UserRegistrationDto;
import rs.ftn.skmd.cinemoj.repository.remote.error_extractor.ErrorExtractor;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.AuthenticationRemoteDao;
import rs.ftn.skmd.cinemoj.usecase.model.Error;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;
import rs.ftn.skmd.cinemoj.usecase.model.Session;


public class AuthenticationRemoteDaoImpl extends BaseRemoteDaoImpl implements AuthenticationRemoteDao {

    private final UnauthenticatedApiService unauthenticatedApiService;

    private final ApiService apiService;

    private final ModelMapper modelMapper;

    public AuthenticationRemoteDaoImpl(UnauthenticatedApiService unauthenticatedApiService, ApiService apiService, ModelMapper modelMapper, ErrorExtractor errorExtractor) {
        super(errorExtractor);
        this.unauthenticatedApiService = unauthenticatedApiService;
        this.apiService = apiService;
        this.modelMapper = modelMapper;
    }

    @Override
    public Single<Result> register(String email, String password, String fullName) {
        UserRegistrationDto userRegistration = new UserRegistrationDto(fullName, email, password);
        return unauthenticatedApiService.registerUser(userRegistration).map(this::convertResponseToResult);
    }

    @Override
    public Single<ResultWithData<Session>> login(String email, String password) {
        UserCredentialsDto userCredentialsDto = new UserCredentialsDto(email, password);

        return unauthenticatedApiService.login(userCredentialsDto)
                .map(this::convertResponseToSessionResponse);
    }

    @Override
    public Single<Result> sendFirebaseRegistrationToken(String token) {
        return apiService.sendFirebaseRegistrationToken(new FirebaseTokenDto(token))
                .map(this::convertResponseToResult);
    }

    private ResultWithData<Session> convertResponseToSessionResponse(Response<UserDto> response) {
        if (response.code() != HttpURLConnection.HTTP_OK || response.body() == null) {
            return new ResultWithData<>(errorExtractor.extractErrorFromResponse(response));
        }

        User user = modelMapper.map(response.body(), User.class);

        String token = response.body().getToken();

        if (token == null || token.isEmpty()) {
            return new ResultWithData<>(new Error("Login failed"));
        }

        return new ResultWithData<>(new Session(token, user));
    }

    @Override
    public Single<Result> deleteFirebaseRegistrationToken(String token) {
        return apiService.deleteFirebaseRegistrationToken()
                .map(this::convertResponseToResult);
    }

    @Override
    public Single<Result> setLanguage(String language) {
        return apiService.setLanguage(language).map(this::convertResponseToResult);
    }
}
