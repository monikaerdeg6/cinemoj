package rs.ftn.skmd.cinemoj.repository.local.database.dao.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.ScreeningDb;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.ScreeningDb.SCREENING_TABLE_NAME;

@Dao
public interface ScreeningDao extends BaseDao<ScreeningDb> {

    @Transaction
    @Query("SELECT * FROM " + SCREENING_TABLE_NAME + " WHERE id = :id LIMIT 1")
    Flowable<ScreeningDb> getById(long id);

    @Transaction
    @Query("SELECT * FROM " + SCREENING_TABLE_NAME)
    Flowable<List<ScreeningDb>> getAll();

    @Query("DELETE FROM " + SCREENING_TABLE_NAME + " WHERE id = :id")
    void deleteById(long id);

    @Query("DELETE FROM " + SCREENING_TABLE_NAME)
    void deleteAll();
}