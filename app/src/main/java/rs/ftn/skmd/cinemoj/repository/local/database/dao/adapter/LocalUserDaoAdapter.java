package rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter;

import org.modelmapper.ModelMapper;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.UserDao;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.UserDb;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalUserDao;

public class LocalUserDaoAdapter implements LocalUserDao {

    private final UserDao userDao;

    private final ModelMapper modelMapper;

    public LocalUserDaoAdapter(UserDao userDao, ModelMapper modelMapper) {
        this.userDao = userDao;
        this.modelMapper = modelMapper;
    }

    @Override
    public Completable save(User user) {
        return Completable.fromAction(() -> {
            UserDb userDb = new UserDb(user.getId(), user.getName(), user.getEmail(),
                    user.getLanguage().ordinal());
            userDao.create(userDb);
//            userDao.create(userDb);
        });
    }

    @Override
    public Flowable<User> getById(long id) {
        return userDao.getById(id).map(this::mapToUser);
    }

    @Override
    public Completable delete(User user) {
        return Completable.fromAction(() -> userDao.deleteById(user.getId()));
    }

    private User mapToUser(UserDb userDb) {
        return new User(userDb.getId(), userDb.getName(), userDb.getUsername(),
                Language.values()[userDb.getLanguage()]);
    }
}
