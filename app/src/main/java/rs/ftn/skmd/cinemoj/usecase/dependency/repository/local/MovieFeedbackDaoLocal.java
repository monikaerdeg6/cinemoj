package rs.ftn.skmd.cinemoj.usecase.dependency.repository.local;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public interface MovieFeedbackDaoLocal {

    Completable save(MovieFeedback movieFeedback);

    Single<List<MovieFeedback>> getAllForMovie(Long movieId);

    Flowable<ResultWithData<MovieFeedback>> getByReservationId(Long reservationId);

}
