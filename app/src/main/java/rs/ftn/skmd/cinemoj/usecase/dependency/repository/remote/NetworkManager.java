package rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote;

public interface NetworkManager {

    boolean isNetworkAvailable();
}
