package rs.ftn.skmd.cinemoj.repository.remote.client.unauthenticated;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.ftn.skmd.cinemoj.BuildConfig;
import rs.ftn.skmd.cinemoj.repository.remote.api.UnauthenticatedApiService;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.repository.remote.client.exception.NoNetworkException;

public class UnauthenticatedHttpClientImpl implements UnauthenticatedHttpClient {

    private final NetworkManager networkManager;

    private UnauthenticatedApiService apiService;

    public UnauthenticatedHttpClientImpl(NetworkManager networkManager) {
        this.networkManager = networkManager;

        apiService = new Retrofit.Builder()
                .baseUrl(BuildConfig.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getClient())
                .build().create(UnauthenticatedApiService.class);
    }

    @Override
    public UnauthenticatedApiService getApiService() {
        return apiService;
    }

    private OkHttpClient getClient() {
        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .addNetworkInterceptor(loggingInterceptor);

        okHttpBuilder.addInterceptor(chain -> {
            if (!networkManager.isNetworkAvailable()) {
                throw new NoNetworkException();
            }
            return chain.proceed(chain.request());
        });

        return okHttpBuilder.build();
    }
}
