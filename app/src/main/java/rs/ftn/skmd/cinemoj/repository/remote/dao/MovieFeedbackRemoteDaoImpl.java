package rs.ftn.skmd.cinemoj.repository.remote.dao;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;

import io.reactivex.Single;
import retrofit2.Response;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.repository.remote.api.ApiService;
import rs.ftn.skmd.cinemoj.repository.remote.dto.MovieFeedbackDTO;
import rs.ftn.skmd.cinemoj.repository.remote.error_extractor.ErrorExtractor;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.MovieFeedbackDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class MovieFeedbackRemoteDaoImpl extends BaseRemoteDaoImpl implements MovieFeedbackDaoRemote {

    private final ApiService apiService;

    private final ModelMapper modelMapper;

    public MovieFeedbackRemoteDaoImpl(ApiService apiService, ModelMapper modelMapper,
                                      ErrorExtractor errorExtractor) {
        super(errorExtractor);
        this.apiService = apiService;
        this.modelMapper = modelMapper;
    }

    @Override
    public Single<Result> save(MovieFeedback movieFeedback) {
        final MovieFeedbackDTO movieFeedbackDTO = new MovieFeedbackDTO(movieFeedback.getUserId(),
                movieFeedback.getReservationId(), movieFeedback.getRate(), movieFeedback.getText());

        return apiService.saveFeedback(movieFeedbackDTO).map(this::convertResponseToResult);
    }

    @Override
    public Single<ResultWithData<MovieFeedback>> getFeedbackForReservation(Long reservationId) {
        return apiService.getReservationFeedback(reservationId)
                .map(this::convertResponseToMovieFeedback);
    }


    private ResultWithData<MovieFeedback> convertResponseToMovieFeedback(
            Response<MovieFeedbackDTO> response) {
        if (response == null || response.body() == null) {
            return null;
        }
        final Type type = new TypeToken<MovieFeedback>() {
        }.getType();
        final MovieFeedback movieFeedback = modelMapper.map(response.body(), type);
        movieFeedback.setReservationId(response.body().getReservation_id());

        return new ResultWithData<>(movieFeedback);
    }

//    private ResultWithData<List<MovieFeedback>> convertResponseToListMovieFeedback(
//            Response<List<MovieFeedbackDTO>> response) {
//
//        if (response == null || response.body() == null) {
//            return null;
//        }
//        final Type type = new TypeToken<List<MovieFeedback>>() {
//        }.getType();
//        final List<MovieFeedback> feedbacks = modelMapper.map(response.body(), type);
//
//        return new ResultWithData<>(feedbacks);
//    }
}
