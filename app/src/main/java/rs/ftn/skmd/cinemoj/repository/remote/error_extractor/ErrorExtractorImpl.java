package rs.ftn.skmd.cinemoj.repository.remote.error_extractor;

import com.google.gson.Gson;

import retrofit2.Response;
import rs.ftn.skmd.cinemoj.usecase.model.Error;

public class ErrorExtractorImpl implements ErrorExtractor {

    private static final String TAG = ErrorExtractorImpl.class.getSimpleName();

    private final Gson gson;

    public ErrorExtractorImpl() {
        this.gson = new Gson();
    }

    @Override
    public Error extractErrorFromResponse(Response response) {
        if (response.errorBody() == null) {
            return new Error(response.message(), response.code());
        }

        Error error;
        try {
            error = gson.fromJson(response.errorBody().charStream(), Error.class);
            error.setCode(response.code());
        } catch (Exception e) {
            error = new Error(response.message(), response.code());
        }

        return error;
    }

}
