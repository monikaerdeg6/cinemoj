package rs.ftn.skmd.cinemoj.system;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;


public class NetworkManagerImpl implements NetworkManager {

    private final Context context;

    NetworkManagerImpl(Context context) {
        this.context = context;
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

