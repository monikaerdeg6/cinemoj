package rs.ftn.skmd.cinemoj.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.ui.adapter.listener.MovieSelectedListener;

@EViewGroup(R.layout.view_item_soon)
public class MovieItemView extends RelativeLayout {

    @ViewById
    TextView movieName;

    @ViewById
    ImageView moviePoster;

    public MovieItemView(Context context) {
        super(context);
    }

    public void bind(Movie movie, MovieSelectedListener movieSelectedListener) {
        movieName.setText(movie.getName());
        loadPosterImage(movie.getPictureUrl());

        this.setOnClickListener(v -> {
            if (movieSelectedListener != null) {
                movieSelectedListener.movieSelected(movie);
            }
        });
    }

    @UiThread
    public void loadPosterImage(String url) {
        if(url == null){
            return;
        }

        Glide.with(this)
                .asBitmap()
                .load(url)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        moviePoster.setImageBitmap(resource);
                    }
                });
    }
}
