package rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor;

import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.SecureStorage;

public class TokenReaderImpl implements TokenReader {

    private final SecureStorage secureStorage;

    public TokenReaderImpl(SecureStorage secureStorage) {
        this.secureStorage = secureStorage;
    }

    @Override
    public String readAuthToken() {
        return secureStorage.getToken();
    }
}
