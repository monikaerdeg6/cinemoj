package rs.ftn.skmd.cinemoj.repository.remote.dao;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.repository.remote.api.ApiService;
import rs.ftn.skmd.cinemoj.repository.remote.dto.MovieDto;
import rs.ftn.skmd.cinemoj.repository.remote.error_extractor.ErrorExtractor;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.MovieDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class MovieRemoteDaoImpl extends BaseRemoteDaoImpl implements MovieDaoRemote {

    private final ApiService apiService;

    private final ModelMapper modelMapper;

    public MovieRemoteDaoImpl(ApiService apiService, ModelMapper modelMapper, ErrorExtractor errorExtractor) {
        super(errorExtractor);
        this.apiService = apiService;
        this.modelMapper = modelMapper;
    }


    @Override
    public Single<ResultWithData<Movie>> getById(Long id) {
        return apiService.getMovieById(id).map(this::convertResponseToMovie);
    }

    @Override
    public Single<ResultWithData<List<Movie>>> getAll() {
        return apiService.getMovies().map(this::convertResponseToMovies);
    }

    private ResultWithData<Movie> convertResponseToMovie(Response<MovieDto> response) {

        if (response == null || response.body() == null) {
            return null;
        }
        final Type type = new TypeToken<Movie>() {
        }.getType();
        final Movie movie = modelMapper.map(response.body(), type);

        return new ResultWithData<>(movie);
    }

    private ResultWithData<List<Movie>> convertResponseToMovies(Response<List<MovieDto>> response) {

        if (response == null || response.body() == null) {
            return null;
        }
        final Type type = new TypeToken<List<Movie>>() {
        }.getType();
        final List<Movie> movies = modelMapper.map(response.body(), type);

        return new ResultWithData<>(movies);
    }

}
