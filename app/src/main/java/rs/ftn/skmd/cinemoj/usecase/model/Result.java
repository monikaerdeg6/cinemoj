package rs.ftn.skmd.cinemoj.usecase.model;


public class Result {

    protected boolean success;
    private Error error;

    public Result() {
        this.success = true;
    }

    public Result(Error error) {
        this.error = error;
        this.success = false;
    }

    public boolean isSuccess() {
        return success;
    }

    public Error getError() {
        return error;
    }
}
