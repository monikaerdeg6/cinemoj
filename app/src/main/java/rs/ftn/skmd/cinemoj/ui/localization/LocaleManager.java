package rs.ftn.skmd.cinemoj.ui.localization;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.Locale;


@EBean
public class LocaleManager {

    @RootContext
    Context context;

    private final String english = "en_EN";

    private final String serbian = "sr_RS";

    public void setSerbian() {
        setLocale(serbian);
    }

    public void setEnglish() {
        setLocale(english);
    }

    private void setLocale(String language) {
        String[] localeStringSplit = language.split("_");
        Locale locale = new Locale(localeStringSplit[0], localeStringSplit[1]);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }
}