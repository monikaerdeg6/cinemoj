package rs.ftn.skmd.cinemoj.repository.remote.client.authenticated;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.ftn.skmd.cinemoj.BuildConfig;
import rs.ftn.skmd.cinemoj.repository.remote.api.ApiService;
import rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor.HttpInterceptor;

public class AuthenticatedHttpClientImpl implements AuthenticatedHttpClient {

    private final HttpInterceptor httpInterceptor;

    private ApiService apiService;

    public AuthenticatedHttpClientImpl(HttpInterceptor httpInterceptor) {
        this.httpInterceptor = httpInterceptor;

        apiService = new Retrofit.Builder()
                .baseUrl(BuildConfig.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getClient())
                .build().create(ApiService.class);
    }

    private OkHttpClient getClient() {
        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .addNetworkInterceptor(loggingInterceptor);

        okHttpBuilder.addInterceptor(httpInterceptor);
        return okHttpBuilder.build();
    }

    @Override
    public ApiService getApiService() {
        return apiService;
    }
}
