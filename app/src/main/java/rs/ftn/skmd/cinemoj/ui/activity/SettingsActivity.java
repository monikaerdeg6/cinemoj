package rs.ftn.skmd.cinemoj.ui.activity;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.ui.localization.LocaleManager;
import rs.ftn.skmd.cinemoj.usecase.AuthenticationUseCase;
import rs.ftn.skmd.cinemoj.usecase.UserUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.Result;

import static android.provider.LiveFolders.INTENT;

@EActivity(R.layout.activity_settings)
public class SettingsActivity extends BaseActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();

    @ViewById
    TextView notificationName;

    @ViewById
    TextView notificationDetails;

    @ViewById
    TextView languageName;

    @ViewById
    TextView languageDetails;

    @ViewById
    Switch notificationSwitch;

    @ViewById
    Spinner languageSpinner;

    @Bean
    LocaleManager localeManager;

    @App
    CinemojApplication cinemojApplication;

    @Inject
    UserUseCase userUseCase;

    @Inject
    AuthenticationUseCase authenticationUseCase;

    final int englishIndex = 0;

    final int serbianIndex = 1;

    boolean init = true;

    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);
        displayDrawer();
        initTitle();
        populateSpinner();
        notificationSwitch.setChecked(userUseCase.isNotificationsEnabled());
    }

    private void initTitle() {
        if (userUseCase.getLoggedInUser() == null) return;
        if (userUseCase.getLanguage() == Language.ENGLISH) {
            setTitle("Settings");
        } else {
            setTitle("Podešavanja");
        }
    }

    @CheckedChange
    void notificationSwitch(boolean checked) {
        userUseCase.setNotificationsEnabled(checked);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(4).setChecked(true);
    }

    private void populateSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.language_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        languageSpinner.setAdapter(adapter);
        languageSpinner.setSelection(userUseCase.getLanguage().ordinal());

        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (init) {
                    init = false;
                    return;
                }

                switch (i) {
                    case englishIndex:
                        localeManager.setEnglish();
                        break;
                    case serbianIndex:
                        localeManager.setSerbian();
                }
                sendLanguage(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void sendLanguage(int languageIndex) {
        Language language = languageIndex == 0  ? Language.ENGLISH : Language.SERBIAN;
        compositeDisposable.add(authenticationUseCase.setLanguage(language)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onLanguageSet, this::onError));

        User user = userUseCase.getLoggedInUser();
        user.setLanguage(language);

        compositeDisposable.add( userUseCase.saveUser(user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe());
    }

    private void onLanguageSet(Result result) {
        refresh();
    }

    private void refresh() {
        finish();
        ScreeningsActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_NO_ANIMATION).start();
    }

}
