package rs.ftn.skmd.cinemoj;

import android.app.Application;

import com.facebook.stetho.Stetho;

import org.androidannotations.annotations.EApplication;

import rs.ftn.skmd.cinemoj.repository.local.LocalRepositoryModule;
import rs.ftn.skmd.cinemoj.repository.remote.RemoteRepositoryModule;
import rs.ftn.skmd.cinemoj.system.SystemModule;
import rs.ftn.skmd.cinemoj.usecase.UseCaseModule;

@EApplication
public class CinemojApplication extends Application {

    private DiComponent diComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);

        diComponent = DaggerDiComponent.builder()
                .useCaseModule(new UseCaseModule())
                .systemModule(new SystemModule(this))
                .remoteRepositoryModule(new RemoteRepositoryModule(this))
                .localRepositoryModule(new LocalRepositoryModule(this))
                .build();
    }

    public DiComponent getDiComponent() {
        return diComponent;
    }
}
