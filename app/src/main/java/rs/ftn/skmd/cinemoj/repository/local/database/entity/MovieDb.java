package rs.ftn.skmd.cinemoj.repository.local.database.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieDb.MOVIE_TABLE_NAME;

@Entity(tableName = MOVIE_TABLE_NAME)
public class MovieDb implements Serializable {

    public static final String MOVIE_TABLE_NAME = "movie";

    @PrimaryKey(autoGenerate = false)
    private Long movieId;

    private String name;

    private String description;

    private int duration;

    private int year;

    private int rating;

    private boolean favorite = false;

    private String genres;

    private String pictureUrl;

    public MovieDb() {
    }

    @Ignore
    public MovieDb(Long movieId, String name, String description, int duration, int year, int rating,
                   boolean favorite, String genres, String pictureUrl) {
        this.movieId = movieId;
        this.name = name;
        this.description = description;
        this.duration = duration;
        this.year = year;
        this.rating = rating;
        this.favorite = favorite;
        this.genres = genres;
        this.pictureUrl = pictureUrl;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
