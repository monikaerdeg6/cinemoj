package rs.ftn.skmd.cinemoj.repository.local.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieFeedbackDb.MOVIE_FEEDBACK_TABLE_NAME;

@Entity(tableName = MOVIE_FEEDBACK_TABLE_NAME)
@ForeignKey(entity = MovieDb.class, parentColumns = "id", childColumns = "movie_id")
public class MovieFeedbackDb {

    public static final String MOVIE_FEEDBACK_TABLE_NAME = "movie_feedback";

    @PrimaryKey(autoGenerate = true)
    private Long id;

    private String username;

    private String text;

    @ColumnInfo(name = "movie_id")
    private Long movieId;

    private Integer rate;

    @ColumnInfo(name="reservation_id")
    private Long reservationId;

    public MovieFeedbackDb() {
    }

    @Ignore
    public MovieFeedbackDb(Long id, String username, String text, Long movieId, Integer rate,
                           Long reservationId) {
        this.id = id;
        this.username = username;
        this.text = text;
        this.movieId = movieId;
        this.rate = rate;
        this.reservationId = reservationId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }
}
