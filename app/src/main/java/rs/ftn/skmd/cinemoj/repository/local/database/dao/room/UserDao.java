package rs.ftn.skmd.cinemoj.repository.local.database.dao.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.UserDb;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.UserDb.USER_TABLE_NAME;

@Dao
public interface UserDao extends BaseDao<UserDb> {

    @Transaction
    @Query("SELECT * FROM " + USER_TABLE_NAME + " WHERE id = :id LIMIT 1")
    Flowable<UserDb> getById(long id);

    @Query("DELETE FROM " + USER_TABLE_NAME + " WHERE id = :id")
    void deleteById(long id);
}
