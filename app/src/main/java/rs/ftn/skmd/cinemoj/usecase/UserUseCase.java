package rs.ftn.skmd.cinemoj.usecase;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalUserDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.PreferenceStorage;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.SecureStorage;

public class UserUseCase {

    private static final String TAG = AuthenticationUseCase.class.getSimpleName();

    private final SecureStorage secureStorage;

    private final PreferenceStorage preferenceStorage;

    private final LocalUserDao localUserDao;

    public UserUseCase(SecureStorage secureStorage, PreferenceStorage preferenceStorage, LocalUserDao localUserDao) {
        this.secureStorage = secureStorage;
        this.preferenceStorage = preferenceStorage;
        this.localUserDao = localUserDao;
    }

    public boolean isLoggedIn() {
        return preferenceStorage.isLoggedIn();
    }

    public void setLoggedIn(boolean isLoggedIn, Long userId) {
        preferenceStorage.setLoggedIn(isLoggedIn);
        if(userId==null){
            return;
        }
        preferenceStorage.setLoggedInUserId(userId);
    }

    public boolean isNotificationsEnabled() {
        return preferenceStorage.isNotificationsEnabled();
    }

    public void setNotificationsEnabled(boolean enabled) {
        preferenceStorage.setNotificationsEnabled(enabled);
    }

    public void saveToken(String token) {
        secureStorage.saveToken(token);
    }

    public Completable saveUser(User user) {
        return Completable.fromAction(() -> {
            localUserDao.save(user).blockingAwait();
        }).subscribeOn(Schedulers.io());
    }

    public String getToken() {
        return secureStorage.getToken();
    }

    public void saveUserId(long userId) {
        secureStorage.saveUserId(userId);
    }

    public long getUserId() {
        return secureStorage.getUserId();
    }

    public Language getLanguage() {
        return localUserDao.getById(getUserId()).blockingFirst().getLanguage();
    }

    public User getLoggedInUser() {
        if (!isLoggedIn()) {
            return null;
        }
        return localUserDao.getById(getUserId()).blockingFirst();
    }

    public void clearAll() {
        secureStorage.clear();
    }

    public String getFirebaseRegistrationTokenToken() {
        return secureStorage.getFirebaseRegistrationTokenToken();
    }

    public void saveFirebaseRegistrationTokenToken(String token) {
        secureStorage.saveFirebaseRegistrationTokenToken(token);
    }
}
