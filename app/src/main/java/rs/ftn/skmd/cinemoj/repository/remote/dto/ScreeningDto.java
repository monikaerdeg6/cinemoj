package rs.ftn.skmd.cinemoj.repository.remote.dto;

import com.google.gson.annotations.SerializedName;

public class ScreeningDto {

    private long id;

    private String date;

    @SerializedName("movieDTO")
    private MovieDto movie;

    private String hall;

    public ScreeningDto() {
    }

    public ScreeningDto(long id, String date, MovieDto movie, String hall) {
        this.id = id;
        this.date = date;
        this.movie = movie;
        this.hall = hall;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public MovieDto getMovie() {
        return movie;
    }

    public void setMovie(MovieDto movie) {
        this.movie = movie;
    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

}
