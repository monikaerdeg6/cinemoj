package rs.ftn.skmd.cinemoj.repository.remote.dao;


import org.modelmapper.ModelMapper;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Single;
import retrofit2.Response;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.repository.remote.api.ApiService;
import rs.ftn.skmd.cinemoj.repository.remote.dto.CommentDTO;
import rs.ftn.skmd.cinemoj.repository.remote.dto.ReservationDTO;
import rs.ftn.skmd.cinemoj.repository.remote.dto.ScreeningDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.converter.DateConverter;
import rs.ftn.skmd.cinemoj.repository.remote.error_extractor.ErrorExtractor;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.ScreeningRemoteDao;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class ScreeningRemoteDaoImpl extends BaseRemoteDaoImpl implements ScreeningRemoteDao {

    private final ApiService apiService;

    private final ErrorExtractor errorExtractor;

    private final ModelMapper modelMapper;

    public ScreeningRemoteDaoImpl(ApiService apiService, ErrorExtractor errorExtractor,
                                  ModelMapper modelMapper) {
        super(errorExtractor);
        this.apiService = apiService;
        this.errorExtractor = errorExtractor;
        this.modelMapper = modelMapper;
    }

    @Override
    public Single<ResultWithData<List<Screening>>> getAll() {
        return apiService.getScreenings()
                .map(this::convertResponseToScreeningsResponse);
    }

    @Override
    public Single<Result> makeReservation(Long screeningId, Integer ticketsNumber){
        final ReservationDTO reservationDTO = new ReservationDTO(screeningId, ticketsNumber);

        return apiService.makeReservation(reservationDTO).map(this::convertResponseToResult);
    }

    @Override
    public Single<ResultWithData<List<MovieFeedback>>> getMovieComments(Long movie_id){
        final List<CommentDTO> commentDTOS = new ArrayList<>();
        return apiService.getMovieComments(movie_id).map(this::convertResponseToCommentsResponse);
    }

    private ResultWithData<List<Screening>> convertResponseToScreeningsResponse(Response<List<ScreeningDto>>
                                                                                        response) {
        if (response.code() != HttpURLConnection.HTTP_OK || response.body() == null) {
            return new ResultWithData<>(errorExtractor.extractErrorFromResponse(response));
        }

        List<Screening> screenings = response.body().stream().map(screeningDto -> {
            Movie movie = modelMapper.map(screeningDto.getMovie(), Movie.class);
            return new Screening(screeningDto.getId(),
                    DateConverter.fromString(screeningDto.getDate()), movie, screeningDto.getHall());
        }).collect(Collectors.toList());

        return new ResultWithData<>(screenings);
    }

    private ResultWithData<List<MovieFeedback>> convertResponseToCommentsResponse(Response<List<CommentDTO>>
                                                                                        response) {
        if (response.code() != HttpURLConnection.HTTP_OK || response.body() == null) {
            return new ResultWithData<>(errorExtractor.extractErrorFromResponse(response));
        }

        List<MovieFeedback> comments = response.body().stream().map(commentDTO -> {
            return new MovieFeedback(null, commentDTO.getText(), null,
                    commentDTO.getRate(), commentDTO.getId(), commentDTO.getUsername());
        }).collect(Collectors.toList());

        return new ResultWithData<>(comments);
    }
}