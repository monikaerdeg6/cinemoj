package rs.ftn.skmd.cinemoj.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.ui.localization.LocaleManager;
import rs.ftn.skmd.cinemoj.usecase.AuthenticationUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @ViewById
    EditText loginEmail;

    @ViewById
    EditText loginPassword;

    @App
    CinemojApplication cinemojApplication;

    @Inject
    AuthenticationUseCase authenticationUseCase;

    @Bean
    LocaleManager localeManager;

    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);
    }

    @Click
    void login() {
        final String email = this.loginEmail.getText().toString();
        final String password = this.loginPassword.getText().toString();

        loginWithEmailAndPassword(email, password);
    }

    @Click
    void registration() {
        RegisterActivity_.intent(this).start();
    }

    private void loginWithEmailAndPassword(String email, String password) {
        compositeDisposable.add(authenticationUseCase.login(email, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleLoginCompleted,
                        this::handleLoginFailed));
    }

    private void handleLoginCompleted(ResultWithData<User> result) {
        if(!result.isSuccess()){
            return;
        }
        User user = result.getValue();
        if (user.getLanguage() == Language.ENGLISH) {
            localeManager.setEnglish();
        } else {
            localeManager.setSerbian();
        }

        final Intent intent = new Intent().putExtra("userEmail", user.getEmail())
                .putExtra("userFullName", user.getName());
        setResult(RESULT_OK, intent);
        finish();
    }

    private void handleLoginFailed(Throwable error) {
        Log.e(TAG, error.getMessage());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
