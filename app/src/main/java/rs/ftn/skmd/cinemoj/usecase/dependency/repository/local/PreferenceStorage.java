package rs.ftn.skmd.cinemoj.usecase.dependency.repository.local;

public interface PreferenceStorage {

    void setLoggedIn(boolean loggedIn);

    boolean isLoggedIn();

    void setNotificationsEnabled(boolean enabled);

    boolean isNotificationsEnabled();

    void setLoggedInUserId(Long userId);

    Long getLoggedInUserId();
}
