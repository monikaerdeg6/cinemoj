package rs.ftn.skmd.cinemoj.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Reservation implements Serializable {

    private Long reservation_id;

    private Long user_id;

    private Integer rate;

    private String text;

    private String hall;

    private Date date;

    private String seats;

    private Movie movie;

    public Reservation() {
    }

    public Reservation(Long reservation_id, Long user_id, Integer rate, String text, String hall,
                       Date date, String seats, Movie movie) {
        this.reservation_id = reservation_id;
        this.user_id = user_id;
        this.rate = rate;
        this.text = text;
        this.hall = hall;
        this.date = date;
        this.seats = seats;
        this.movie = movie;
    }

    public Long getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(Long reservation_id) {
        this.reservation_id = reservation_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
