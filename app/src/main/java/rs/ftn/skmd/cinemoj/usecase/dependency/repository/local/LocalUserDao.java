package rs.ftn.skmd.cinemoj.usecase.dependency.repository.local;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.domain.User;

public interface LocalUserDao {

    Completable save(User user);

    Flowable<User> getById(long id);

    Completable delete(User user);
}
