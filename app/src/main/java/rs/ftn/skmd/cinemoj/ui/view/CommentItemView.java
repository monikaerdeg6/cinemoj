package rs.ftn.skmd.cinemoj.ui.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;

@EViewGroup(R.layout.view_item_comment)
public class CommentItemView extends LinearLayout {

    @ViewById
    TextView username;

    @ViewById
    TextView text;

    public CommentItemView(Context context) {
        super(context);
    }

    public void bind(MovieFeedback movieFeedback) {
        this.text.setText(movieFeedback.getText());
        this.username.setText(movieFeedback.getUsername());
    }
}
