package rs.ftn.skmd.cinemoj.usecase.model;

public class ResultWithData<T> extends Result {

    private T value;

    public ResultWithData(T value) {
        this.value = value;
        this.success = true;
    }

    public ResultWithData(Error error) {
        super(error);
    }

    public T getValue() {
        return value;
    }

}
