package rs.ftn.skmd.cinemoj.domain;

import java.io.Serializable;

public class MovieFeedback implements Serializable {

    private Long id;

    private Long userId;

    private String text;

    private Long reservationId;

    private Integer rate;

    private String username;

    public MovieFeedback() {
    }

    public MovieFeedback(Long userId, String text, Long reservationId, Integer rate, Long id,
                         String username) {
        this.userId = userId;
        this.text = text;
        this.reservationId = reservationId;
        this.rate = rate;
        this.id = id;
        this.username = username;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
