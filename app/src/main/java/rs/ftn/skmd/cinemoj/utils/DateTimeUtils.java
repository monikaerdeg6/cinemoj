package rs.ftn.skmd.cinemoj.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtils {

    private static SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy, HH:mm", Locale.US);

    public static String format(Date date){
        return DATE_TIME_FORMAT.format(date);
    }
}
