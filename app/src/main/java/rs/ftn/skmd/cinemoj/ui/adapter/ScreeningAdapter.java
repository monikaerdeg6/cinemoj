package rs.ftn.skmd.cinemoj.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.ui.adapter.generic.RecyclerViewAdapterBase;
import rs.ftn.skmd.cinemoj.ui.adapter.listener.ScreeningSelectedListener;
import rs.ftn.skmd.cinemoj.ui.view.ScreeningItemView;
import rs.ftn.skmd.cinemoj.ui.view.ScreeningItemView_;
import rs.ftn.skmd.cinemoj.ui.view.generic.ViewWrapper;

@EBean
public class ScreeningAdapter extends RecyclerViewAdapterBase<Screening, ScreeningItemView> {

    @RootContext
    Context context;

    private ScreeningSelectedListener screeningSelectedListener;

    @Override
    public ScreeningItemView onCreateItemView(ViewGroup parent, int viewType) {
        return ScreeningItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ScreeningItemView> viewHolder, int position) {
        ScreeningItemView view = viewHolder.getView();
        final Screening screening = items.get(position);
        view.bind(screening, screeningSelectedListener);
    }

    public void setList(List<Screening> screenings) {
        this.items = screenings;
        notifyDataSetChanged();
    }

    public void setScreeningSelectedListener(ScreeningSelectedListener screeningSelectedListener) {
        this.screeningSelectedListener = screeningSelectedListener;
    }

    public void filterScreenings(String searchInput) {
        //setList(utility.searchHome(searchInput, items));
    }
}