package rs.ftn.skmd.cinemoj.repository.local.database.dao.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.ReservationDb;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.ReservationDb.RESERVATION_TABLE_NAME;

@Dao
public interface ReservationDao extends BaseDao<ReservationDb> {

    @Transaction
    @Query("SELECT * FROM " + RESERVATION_TABLE_NAME + " WHERE movieId = :id LIMIT 1")
    Flowable<ReservationDb> getById(long id);

    @Query("SELECT * FROM " + RESERVATION_TABLE_NAME)
    Flowable<List<ReservationDb>> getAll();

    @Query("DELETE FROM " + RESERVATION_TABLE_NAME)
    void deleteAll();
}
