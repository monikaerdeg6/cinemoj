package rs.ftn.skmd.cinemoj.repository.remote.client.exception;

public class NoNetworkException extends RuntimeException {

    @Override
    public String getMessage() {
        return "No Network";
    }
}
