package rs.ftn.skmd.cinemoj.repository.local.keychain;

import java.util.ArrayList;
import java.util.List;

import de.adorsys.android.securestoragelibrary.SecurePreferences;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.SecureStorage;

public class SecureStorageImpl implements SecureStorage {
    
    private static final String TOKEN = "token";
    
    private static final String FIREBASE_REGISTRATION_TOKEN = "firebase-registration-token";

    private static final String USER_ID = "user-id";

    private static final int CHUNK_SIZE = 200;

    @Override
    public void saveToken(String token) {
        set(TOKEN, token);
    }

    @Override
    public String getToken() {
        return get(TOKEN);
    }

    @Override
    public void removeToken() {
        remove(TOKEN);
    }

    @Override
    public void saveFirebaseRegistrationTokenToken(String firebaseRegistrationToken) {
        set(FIREBASE_REGISTRATION_TOKEN, firebaseRegistrationToken);
    }

    @Override
    public String getFirebaseRegistrationTokenToken() {
        return get(FIREBASE_REGISTRATION_TOKEN);
    }

    @Override
    public void removeFirebaseRegistrationTokenToken() {
        remove(FIREBASE_REGISTRATION_TOKEN);
    }

    @Override
    public void saveUserId(long userId) {
        set(USER_ID, String.valueOf(userId));
    }

    @Override
    public long getUserId() {
        return Long.valueOf(get(USER_ID));
    }

    @Override
    public void removeUserId() {
        remove(USER_ID);
    }

    @Override
    public void clear() {
        SecurePreferences.clearAllValues();
    }

    private void set(String key, String value) {
        if (value.length() < CHUNK_SIZE) {
            SecurePreferences.setValue(key, value);
            return;
        }
        setLongStringValue(key, value);
    }

    private String get(String key) {
        final String DEFAULT_VALUE = "";
        if (SecurePreferences.contains(key)) {
            return SecurePreferences.getStringValue(key, DEFAULT_VALUE);
        }
        return getLongStringValue(key, DEFAULT_VALUE);
    }

    private void remove(String key) {
        if (SecurePreferences.contains(key)) {
            SecurePreferences.removeValue(key);
            return;
        }
        removeLongStringValue(key);
    }

    private String getNumberOfChunksKey(String key) {
        return key + "_numberOfChunks";
    }

    private void setLongStringValue(String key, String value) {
        List<String> chunks = new ArrayList<>();

        int len = value.length();

        for (int i = 0; i < len; i += CHUNK_SIZE) {
            chunks.add(value.substring(i, Math.min(len, i + CHUNK_SIZE)));
        }

        SecurePreferences.setValue(getNumberOfChunksKey(key), chunks.size());

        for (int i = 0; i < chunks.size(); i++) {
            SecurePreferences.setValue(key + i, chunks.get(i));
        }
    }

    private String getLongStringValue(String key, String defaultValue) {
        int numberOfChunks = SecurePreferences.getIntValue(getNumberOfChunksKey(key), 0);

        if (numberOfChunks == 0) {
            return defaultValue;
        }

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < numberOfChunks; i++) {
            result.append(SecurePreferences.getStringValue(key + i, ""));
        }

        return result.toString();
    }

    private void removeLongStringValue(String key) {

        int numberOfChunks = SecurePreferences.getIntValue(getNumberOfChunksKey(key), 0);

        for (int i = 0; i < numberOfChunks; i++) {
            SecurePreferences.removeValue(key + i);
        }

        SecurePreferences.removeValue(getNumberOfChunksKey(key));
    }
}