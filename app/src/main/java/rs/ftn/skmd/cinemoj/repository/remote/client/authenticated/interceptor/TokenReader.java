package rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor;

public interface TokenReader {

    String readAuthToken();
}
