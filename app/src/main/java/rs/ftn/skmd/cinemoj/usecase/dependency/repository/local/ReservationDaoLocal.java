package rs.ftn.skmd.cinemoj.usecase.dependency.repository.local;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public interface ReservationDaoLocal {

    Completable save(Reservation reservation);

    Flowable<ResultWithData<Reservation>> getById(Long id);

    Flowable<ResultWithData<List<Reservation>>> getAll();

    Completable deleteAll();

    Completable saveAll(List<Reservation> reservations);

}
