package rs.ftn.skmd.cinemoj.repository.remote.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rs.ftn.skmd.cinemoj.repository.remote.dto.CommentDTO;
import rs.ftn.skmd.cinemoj.repository.remote.dto.FirebaseTokenDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.MovieDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.MovieFeedbackDTO;
import rs.ftn.skmd.cinemoj.repository.remote.dto.ReservationDTO;
import rs.ftn.skmd.cinemoj.repository.remote.dto.ScreeningDto;

public interface ApiService {

    @POST("user/firebaseRegistrationToken")
    Single<Response<Void>> sendFirebaseRegistrationToken(@Body FirebaseTokenDto token);

    @DELETE("user/logout")
    Single<Response<Void>> deleteFirebaseRegistrationToken();

    @PUT("user/language/{lang}")
    Single<Response<Void>> setLanguage(@Path("lang") String language);

    @GET("screening/all")
    Single<Response<List<ScreeningDto>>> getScreenings();

    @POST("screening/reserve")
    Single<Response<Void>> makeReservation(@Body ReservationDTO reservationDTO);

    @GET("movie/soon/{id}")
    Single<Response<MovieDto>> getMovieById(@Path("id") Long id);

    @GET("movie/soon")
    Single<Response<List<MovieDto>>> getMovies();

    @GET("reservation/{reservationId}")
    Single<Response<MovieFeedbackDTO>> getReservationFeedback(@Path("reservationId") Long reservationId);

    @POST("reservation/saveFeedback")
    Single<Response<Void>> saveFeedback(@Body MovieFeedbackDTO movieFeedbackDto);

    @GET("reservation/{reservation_id}")
    Single<Response<MovieFeedbackDTO>> getMovieFeedback(@Path("reservation_id") Long id);

    @GET("reservation/all")
    Single<Response<List<MovieFeedbackDTO>>> getMyReservations();

    @GET("comment/all/{movie_id}")
    Single<Response<List<CommentDTO>>> getMovieComments(@Path("movie_id") Long movie_id);
}
