package rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter;

import org.modelmapper.ModelMapper;
import org.reactivestreams.Publisher;

import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.MovieDao;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieDb;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.MovieDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class MovieDaoAdapter implements MovieDaoLocal {

    private final ModelMapper modelMapper;

    private final MovieDao movieDao;

    public MovieDaoAdapter(MovieDao movieDao, ModelMapper modelMapper) {
        this.movieDao = movieDao;
        this.modelMapper = modelMapper;
    }

    @Override
    public Completable save(Movie movie) {
        return Completable.fromAction(() -> {
            MovieDb movieDb = modelMapper.map(movie, MovieDb.class);
            movieDao.create(movieDb);
        });
    }

    @Override
    public Completable saveAll(List<Movie> movies) {
        return Completable.fromAction(() -> {
            List<MovieDb> movieDbs = movies.stream().map(movie ->
                    new MovieDb(movie.getId(), movie.getName(),
                            movie.getDescription(), movie.getDuration(),
                            movie.getYear(), movie.getRating(),
                            movie.isFavorite(), movie.getGenres(),
                            movie.getPictureUrl())).collect(Collectors.toList());
            this.movieDao.createAll(movieDbs);

        });
    }


    @Override
    public Flowable<ResultWithData<Movie>> getById(Long id) {
        return movieDao.getById(id).map(this::mapToMovie).map(this::mapToResultById);
    }

    @Override
    public Flowable<ResultWithData<List<Movie>>> getAll() {
        return movieDao.getSoon().concatMap((Function<List<MovieDb>,
                Publisher<ResultWithData<List<Movie>>>>) galleryWithImages ->
                Flowable.fromIterable(galleryWithImages)
                        .map(this::mapToMovie)
                        .toList()
                        .map(this::mapToResult)
                        .toFlowable());
    }

    @Override
    public Completable deleteAll() {
        return Completable.fromAction(() -> movieDao.deleteAll());
    }

    @Override
    public Completable delete(Long id) {
        return Completable.fromAction(() -> movieDao.deleteById(id));
    }

    private Movie mapToMovie(MovieDb movieDb) {
        final Movie movie = modelMapper.map(movieDb, Movie.class);
        return movie;
    }

    private ResultWithData<List<Movie>> mapToResult(List<Movie> movies) {
        return new ResultWithData<>(movies);
    }

    private ResultWithData<Movie> mapToResultById(Movie movie) {
        return new ResultWithData<>(movie);
    }
}
