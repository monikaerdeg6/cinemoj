package rs.ftn.skmd.cinemoj.usecase.dependency.repository.local;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public interface MovieDaoLocal {

    Completable save(Movie movie);

    Completable saveAll(List<Movie> movies);

    Flowable<ResultWithData<Movie>> getById(Long id);

    Flowable<ResultWithData<List<Movie>>> getAll();

    Completable deleteAll();

    Completable delete(Long id);

}
