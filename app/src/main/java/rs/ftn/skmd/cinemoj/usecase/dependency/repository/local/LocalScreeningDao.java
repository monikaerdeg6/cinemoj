package rs.ftn.skmd.cinemoj.usecase.dependency.repository.local;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public interface LocalScreeningDao {

    Completable save(Screening screening);

    Flowable<Screening> getById(long id);

    Flowable<ResultWithData<List<Screening>>> getAll();

    Completable delete(Screening screening);

    Completable deleteAll();

    Completable saveAll(List<Screening> screenings);
}
