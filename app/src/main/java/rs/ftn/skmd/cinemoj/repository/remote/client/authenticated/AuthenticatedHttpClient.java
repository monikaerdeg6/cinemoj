package rs.ftn.skmd.cinemoj.repository.remote.client.authenticated;

import rs.ftn.skmd.cinemoj.repository.remote.api.ApiService;

public interface AuthenticatedHttpClient {
    ApiService getApiService();
}
