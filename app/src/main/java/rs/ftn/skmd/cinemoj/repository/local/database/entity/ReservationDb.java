package rs.ftn.skmd.cinemoj.repository.local.database.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.RoomWarnings;

import java.time.LocalDateTime;
import java.util.Date;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.ReservationDb.RESERVATION_TABLE_NAME;

@Entity(tableName = RESERVATION_TABLE_NAME)
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
public class ReservationDb {

    public static final String RESERVATION_TABLE_NAME = "reservation";

    @PrimaryKey(autoGenerate = true)
    private Long reservation_id;

    private Long user_id;

    private Integer rate;

    private String text;

    private String hall;

    private Date date;

    private String seats;

    @Embedded
    private MovieDb movie;

    public ReservationDb() {
    }

    @Ignore
    public ReservationDb(Long reservation_id, Long user_id, Integer rate, String text, String hall,
                         Date date, String seats, MovieDb movie) {
        this.reservation_id = reservation_id;
        this.user_id = user_id;
        this.rate = rate;
        this.text = text;
        this.hall = hall;
        this.date = date;
        this.seats = seats;
        this.movie = movie;
    }

    public Long getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(Long reservation_id) {
        this.reservation_id = reservation_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public MovieDb getMovie() {
        return movie;
    }

    public void setMovie(MovieDb movie) {
        this.movie = movie;
    }
}
