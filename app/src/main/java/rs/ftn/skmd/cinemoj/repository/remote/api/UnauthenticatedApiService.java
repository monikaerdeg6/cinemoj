package rs.ftn.skmd.cinemoj.repository.remote.api;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rs.ftn.skmd.cinemoj.repository.remote.dto.UserCredentialsDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.UserDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.UserRegistrationDto;

public interface UnauthenticatedApiService {

    @POST("user/register")
    Single<Response<Void>> registerUser(@Body UserRegistrationDto userRegistration);

    @POST("user/login")
    Single<Response<UserDto>> login(@Body UserCredentialsDto userCredentials);
}
