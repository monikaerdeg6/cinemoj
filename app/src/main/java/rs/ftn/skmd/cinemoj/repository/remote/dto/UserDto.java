package rs.ftn.skmd.cinemoj.repository.remote.dto;

public class UserDto {

    private Long id;

    private String name;

    private String email;

    private String token;

    private String language;

    public UserDto() {
    }

    public UserDto(Long id, String name, String email, String token, String language) {
        this.name = name;
        this.id = id;
        this.email = email;
        this.token = token;
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
