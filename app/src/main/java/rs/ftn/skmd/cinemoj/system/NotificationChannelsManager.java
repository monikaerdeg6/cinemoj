package rs.ftn.skmd.cinemoj.system;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

class NotificationChannelsManager {

    private final Context context;

    NotificationChannelsManager(Context context) {
        this.context = context;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    void createChannel(String channelId, String channelName) {
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager service = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (service != null) {
            service.createNotificationChannel(channel);
        }
    }

}
