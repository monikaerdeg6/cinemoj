package rs.ftn.skmd.cinemoj.ui.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.ui.adapter.ScreeningAdapter;
import rs.ftn.skmd.cinemoj.ui.localization.LocaleManager;
import rs.ftn.skmd.cinemoj.usecase.ScreeningUseCase;
import rs.ftn.skmd.cinemoj.usecase.UserUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

@EActivity(R.layout.activity_screenings)
public class ScreeningsActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {

    private static final String TAG = ScreeningsActivity.class.getSimpleName();

    public static final int LOGIN_REQUEST = 123;

    @ViewById
    EditText dateField;

    @ViewById
    EditText fromTimeField;

    @ViewById
    EditText toTimeField;

    @Bean
    ScreeningAdapter adapter;

    @Bean
    LocaleManager localeManager;

    @ViewById
    RecyclerView recyclerView;

    @Inject
    UserUseCase userUseCase;

    @App
    CinemojApplication cinemojApplication;

    @Inject
    ScreeningUseCase screeningUseCase;

    private DatePickerDialog datePickerDialog;

    private TimePickerDialog timePickerDialog;

    private boolean fromTimeSelected = false;

    private List<Screening> screenings = new ArrayList<>();

    private List<Screening> allScreenings = new ArrayList<>();


    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);

        User user = userUseCase.getLoggedInUser();
        if (user != null) {
            if (user.getLanguage() == Language.ENGLISH) {
                localeManager.setEnglish();
            } else {
                localeManager.setSerbian();
            }
        }

        displayDrawer();
        setupPickers();

        if (user != null) {
            setTitle(user.getLanguage() == Language.ENGLISH ? "Screenings" : "Projekcije");
        }
    }

    private void loadScreenings() {
        compositeDisposable.add(screeningUseCase.getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleScreeningsFetched, this::onError));
    }

    private void handleScreeningsFetched(ResultWithData<List<Screening>> result) {
        if (!result.isSuccess()) {
            showToast("Error!", Toast.LENGTH_SHORT);
            return;
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false));
        recyclerView.setAdapter(adapter);
        screenings = result.getValue();
        allScreenings = result.getValue();
        adapter.setList(screenings);

        adapter.setScreeningSelectedListener(screening ->
                ScreeningDetailsActivity_.intent(this).screening(screening).start());
    }

    private boolean checkForUser() {
        if (!userUseCase.isLoggedIn()) {
            LoginActivity_.intent(this).startForResult(LOGIN_REQUEST);
            return false;
        }
        return true;
    }

    private void setupPickers() {
        Calendar calendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(
                this, this, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        timePickerDialog = new TimePickerDialog(this, this,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkForUser()) {
            return;
        }
        if (allScreenings.isEmpty()) {
            loadScreenings();
        }
        setTitle(R.string.screenings);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    @Click
    void dateField() {
        datePickerDialog.show();
    }

    @Click
    void fromTimeField() {
        fromTimeSelected = true;
        timePickerDialog.show();
    }

    @Click
    void toTimeField() {
        fromTimeSelected = false;
        timePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        dateField.setText(String.format("%s/%s/%s", String.valueOf(i2), String.valueOf(i1 + 1),
                String.valueOf(i)));
        if(!fromTimeField.getText().toString().equals("") && !toTimeField.getText().toString()
                .equals("")){
            filterScreenings();
        }
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1){
        String time = String.format("%s:%s", String.valueOf(i), String.valueOf(i1));
        if (fromTimeSelected) {
            fromTimeField.setText(time);
        } else {
            toTimeField.setText(time);
        }
        if(!fromTimeField.getText().toString().equals("") && !toTimeField.getText().toString()
                .equals("") && !dateField.getText().toString().equals("")){
            filterScreenings();
        }
    }

    @OnActivityResult(LOGIN_REQUEST)
    void userLoggedIn(Intent intent) {
        displayDrawer();
        setupPickers();
        loadScreenings();
        setUser();
    }

    private Date makeDate(String date, String time){
        Date dateValue = null;
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE);
            dateValue = formatter.parse(date + " " + time);
        } catch (ParseException e){
            e.printStackTrace();
        }

        return dateValue;
    }

    private void filterScreenings(){
        screenings = screeningUseCase.filterScreenings(allScreenings,
                makeDate(dateField.getText().toString(), fromTimeField.getText().toString()),
                makeDate(dateField.getText().toString(), toTimeField.getText().toString()));
        adapter.setList(screenings);
    }

    @Override
    public void onBackPressed() {
        if(dateField.getText().toString().isEmpty()) {
            super.onBackPressed();
            return;
        }
        dateField.setText("");
        fromTimeField.setText("");
        toTimeField.setText("");
        screenings = new ArrayList<>(allScreenings);
        adapter.setList(screenings);
    }
}
