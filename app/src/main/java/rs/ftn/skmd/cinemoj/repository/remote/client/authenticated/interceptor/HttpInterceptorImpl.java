package rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.repository.remote.client.exception.NoNetworkException;

public class HttpInterceptorImpl implements HttpInterceptor {

    private static final String TAG = HttpInterceptor.class.getSimpleName();
    private static final String AUTHORIZATION = "Authorization";
    private static final String CONNECTION = "Connection";
    private static final String CONNECTION_VALUE = "close";

    private final NetworkManager networkManager;
    private final TokenReader tokenReader;

    public HttpInterceptorImpl(NetworkManager networkManager, TokenReader tokenReader) {
        this.networkManager = networkManager;
        this.tokenReader = tokenReader;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException, NoNetworkException {

        final String token = tokenReader.readAuthToken();

        Request request = chain.request().newBuilder()
                .addHeader(CONNECTION, CONNECTION_VALUE)
                .addHeader(AUTHORIZATION, token).build();

        if (!networkManager.isNetworkAvailable()) {
            throw new NoNetworkException();
        }
        return chain.proceed(request);
    }
}