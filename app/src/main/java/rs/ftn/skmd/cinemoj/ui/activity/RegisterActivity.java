package rs.ftn.skmd.cinemoj.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.usecase.AuthenticationUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.Result;

@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();

    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @ViewById
    EditText fullName;

    @ViewById
    EditText email;

    @ViewById
    EditText password;

    @ViewById
    EditText confirmPassword;

    @App
    CinemojApplication cinemojApplication;

    @Inject
    AuthenticationUseCase authenticationUseCase;

    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);
    }

    @Click
    void register() {
        if (!confirmPassword.getText().toString().equals(password.getText().toString())) {
            showToast(getString(R.string.passwords_dont_match), Toast.LENGTH_SHORT);
            return;
        }

        if (password.getText().length() < 8) {
            showToast(getString(R.string.password_too_short), Toast.LENGTH_SHORT);
            return;
        }

        register(fullName.getText().toString(), email.getText().toString(),
                password.getText().toString());
    }

    private void register(String name, String email, String password) {
        compositeDisposable.add(authenticationUseCase.registerUser(email, password, name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleRegisterCompleted,
                        this::handleRegisterFailed));
    }

    private void handleRegisterCompleted(Result result) {
        finish();
    }

    private void handleRegisterFailed(Throwable error) {
        Log.e(TAG, error.getMessage());
    }

    public void showToast(String text, int length) {
        Toast.makeText(this, text, length).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
