package rs.ftn.skmd.cinemoj.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.ui.adapter.generic.RecyclerViewAdapterBase;
import rs.ftn.skmd.cinemoj.ui.adapter.listener.MovieSelectedListener;
import rs.ftn.skmd.cinemoj.ui.view.MovieItemView;
import rs.ftn.skmd.cinemoj.ui.view.MovieItemView_;
import rs.ftn.skmd.cinemoj.ui.view.generic.ViewWrapper;

@EBean
public class SoonAdapter extends RecyclerViewAdapterBase<Movie, MovieItemView> {

    @RootContext
    Context context;

    private MovieSelectedListener movieSelectedListener;

    @Override
    public MovieItemView onCreateItemView(ViewGroup parent, int viewType) {
        return MovieItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<MovieItemView> viewHolder, int position) {
        MovieItemView view = viewHolder.getView();
        final Movie movie = items.get(position);
        view.bind(movie, movieSelectedListener);
    }

    public void setList(List<Movie> movies) {
        this.items = movies;
        notifyDataSetChanged();
    }

    public void setMovieSelectedListener(MovieSelectedListener movieSelectedListener) {
        this.movieSelectedListener = movieSelectedListener;
    }
}
