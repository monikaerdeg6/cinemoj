package rs.ftn.skmd.cinemoj.repository.local.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.repository.local.database.converter.DateTypeConverter;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.MovieDao;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.MovieFeedbackDao;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.ReservationDao;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.ScreeningDao;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.UserDao;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieDb;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieFeedbackDb;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.ReservationDb;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.ScreeningDb;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.UserDb;


@Database(entities = {UserDb.class, MovieDb.class, ScreeningDb.class, ReservationDb.class,
        MovieFeedbackDb.class}, version = 1, exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "cinemoj_db";

    public abstract UserDao userDao();

    public abstract MovieDao movieDao();

    public abstract ScreeningDao screeningDao();

    public abstract ReservationDao reservationDao();

    public abstract MovieFeedbackDao movieFeedbackDao();
}
