package rs.ftn.skmd.cinemoj.repository.remote.dto;

public class FirebaseTokenDto {

    private String token;

    public FirebaseTokenDto() {}

    public FirebaseTokenDto(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
