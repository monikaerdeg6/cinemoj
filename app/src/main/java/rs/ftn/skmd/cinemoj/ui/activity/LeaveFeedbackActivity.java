package rs.ftn.skmd.cinemoj.ui.activity;

import android.graphics.PorterDuff;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.ViewById;

import java.util.Date;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.ui.fragment.MovieInfoFragment;
import rs.ftn.skmd.cinemoj.usecase.MovieFeedbackUseCase;
import rs.ftn.skmd.cinemoj.usecase.UserUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

@EActivity(R.layout.activity_leave_feedback)
public class LeaveFeedbackActivity extends BaseActivity {

    private static final String TAG = SoonActivity.class.getSimpleName();

    @Extra
    Reservation reservation;

    @FragmentById
    MovieInfoFragment movieInfoFragment;

    @ViewById
    EditText movieComment;

    @ViewById
    RatingBar myRating;

    @ViewById
    Button postFeedback;

    @App
    CinemojApplication cinemojApplication;

    @Inject
    MovieFeedbackUseCase movieFeedbackUseCase;

    @Inject
    UserUseCase userUseCase;

    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);
        if (ifDatePassed()) {
            loadFeedback();
        } else {
            disableFeedback();
            showToast(getString(R.string.rate_after_screening), Toast.LENGTH_LONG);
        }

        displayDrawer();
        initTitle();
        movieInfoFragment.bind(reservation.getMovie(), this);
    }

    private void initTitle() {
        if (userUseCase.getLoggedInUser() == null) return;
        if (userUseCase.getLanguage() == Language.ENGLISH) {
            setTitle("Feedback");
        } else {
            setTitle("Ocena");
        }
    }

    @Click
    void postFeedback() {
        compositeDisposable.add(movieFeedbackUseCase.leaveFeedback(reservation.getReservation_id(),
                movieComment.getText().toString(), Math.round(myRating.getRating()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleSuccess,
                        this::handleDataLoadFailed));
    }

    void loadFeedback() {
        compositeDisposable.add(movieFeedbackUseCase
                .getMovieFeedbackByReservationId(reservation.getReservation_id())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleDataLoaded,
                        this::handleDataLoadFailed));
    }

    private void handleSuccess(Result result) {
        showToast(getResources().getString(R.string.thank_you_feedback), Toast.LENGTH_SHORT);
        finish();
    }

    private void handleDataLoaded(ResultWithData<MovieFeedback> feedback) {
        if (feedback == null || feedback.getValue() == null
                || (feedback.getValue().getText() == null || feedback.getValue().getText().equals(""))
                && (feedback.getValue().getRate() == null || feedback.getValue().getRate() == 0)) {
            return;
        }
        final MovieFeedback movieFeedback = feedback.getValue();
        if (movieFeedback.getRate() != null) {
            myRating.setRating(feedback.getValue().getRate());

        }
        if (movieFeedback.getText() != null) {
            movieComment.setText(feedback.getValue().getText());
        }
        disableFeedback();
        showToast(getString(R.string.already_rated_message), Toast.LENGTH_LONG);

    }

    private void handleDataLoadFailed(Throwable error) {
        Log.e(TAG, error.getMessage());
    }

    private boolean ifDatePassed() {
        return new Date().after(reservation.getDate());
    }

    private void disableFeedback() {
        myRating.setIsIndicator(true);

        movieComment.setClickable(false);
        movieComment.setFocusable(false);
        movieComment.setEnabled(false);

        postFeedback.setClickable(false);
        postFeedback.getBackground().setColorFilter(getColor(R.color.colorLightGray), PorterDuff.Mode.MULTIPLY);
    }

}
