package rs.ftn.skmd.cinemoj.repository.local.shared_preference;

import android.content.SharedPreferences;

import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.PreferenceStorage;

public class PreferenceStorageImpl implements PreferenceStorage {

    private static final String LOGGED_IN = "isLoggedIn";
    private static final String NOTIFICATIONS_ENABLED = "isNotificationsEnabled";
    private static final String LOGGED_IN_USER_ID = "userId";

    private SharedPreferences sharedPreferences;

    public PreferenceStorageImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void setLoggedIn(boolean loggedIn) {
        sharedPreferences.edit().putBoolean(LOGGED_IN, loggedIn).apply();
    }

    @Override
    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(LOGGED_IN, false);
    }

    @Override
    public void setNotificationsEnabled(boolean enabled) {
        sharedPreferences.edit().putBoolean(NOTIFICATIONS_ENABLED, enabled).apply();
    }

    @Override
    public boolean isNotificationsEnabled() {
        return sharedPreferences.getBoolean(NOTIFICATIONS_ENABLED, false);
    }

    @Override
    public void setLoggedInUserId(Long userId) {
        sharedPreferences.edit().putLong(LOGGED_IN_USER_ID, userId).apply();;
    }

    @Override
    public Long getLoggedInUserId() {
        return sharedPreferences.getLong(LOGGED_IN_USER_ID, -1);
    }
}
