package rs.ftn.skmd.cinemoj.usecase;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.ReservationDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.ReservationDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class ReservationUseCase {

    private final ReservationDaoLocal reservationDaoLocal;

    private final ReservationDaoRemote reservationDaoRemote;

    private final NetworkManager networkManager;

    public ReservationUseCase(ReservationDaoLocal reservationDaoLocal, ReservationDaoRemote reservationDaoRemote,
                              NetworkManager networkManager) {
        this.reservationDaoLocal = reservationDaoLocal;
        this.reservationDaoRemote = reservationDaoRemote;
        this.networkManager = networkManager;
    }

    public Flowable<ResultWithData<Reservation>> getReservationById(Long id) {
        if (!networkManager.isNetworkAvailable()) {
            return reservationDaoLocal.getById(id);
        }
        return this.reservationDaoRemote.getById(id).doOnSuccess(result -> handleGetReservationById(result, id))
                .subscribeOn(Schedulers.io()).toFlowable();
    }


    public Flowable<ResultWithData<List<Reservation>>> getAllReservations() {
        if (!networkManager.isNetworkAvailable()) {
            return reservationDaoLocal.getAll();
        }
        return this.reservationDaoRemote.getAll()
                .map(this::handleGetAllReservationsResult)
                .subscribeOn(Schedulers.io()).toFlowable();
    }

    private ResultWithData<List<Reservation>> handleGetAllReservationsResult(ResultWithData<List<Reservation>> result) {
        if (!result.isSuccess()) {
            return new ResultWithData<>(result.getError());
        }

        reservationDaoLocal.deleteAll().blockingAwait();
        reservationDaoLocal.saveAll(result.getValue()).blockingAwait();
        return new ResultWithData<>(result.getValue());
    }

    private ResultWithData<Reservation> handleGetReservationById(ResultWithData<Reservation> result, Long id) {
        if (result.isSuccess()) {
            reservationDaoLocal.save(result.getValue());
            return result;
        }

        return reservationDaoLocal.getById(id).blockingFirst();
    }
}
