package rs.ftn.skmd.cinemoj.repository.local;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;

import org.modelmapper.ModelMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rs.ftn.skmd.cinemoj.repository.local.database.AppDatabase;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter.LocalScreeningDaoAdapter;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter.LocalUserDaoAdapter;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter.MovieDaoAdapter;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter.MovieFeedbackDaoAdapter;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter.ReservationDaoLocalAdapter;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.UserDao;
import rs.ftn.skmd.cinemoj.repository.local.keychain.SecureStorageImpl;
import rs.ftn.skmd.cinemoj.repository.local.shared_preference.PreferenceStorageImpl;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalScreeningDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalUserDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.MovieDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.MovieFeedbackDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.PreferenceStorage;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.ReservationDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.SecureStorage;

@Module
public class LocalRepositoryModule {

    private final Context context;

    public LocalRepositoryModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    SecureStorage provideKeychain() {
        return new SecureStorageImpl();
    }

    @Provides
    @Singleton
    PreferenceStorage providePreferences(SharedPreferences sharedPreferences) {
        return new PreferenceStorageImpl(sharedPreferences);
    }

    @Provides
    @Singleton
    AppDatabase providesDatabase() {
        return Room.databaseBuilder(context, AppDatabase.class, AppDatabase.DATABASE_NAME).fallbackToDestructiveMigration().build();
    }

    @Provides
    @Singleton
    LocalUserDao providesLocalUserDao(AppDatabase appDatabase, ModelMapper modelMapper) {
        return new LocalUserDaoAdapter(appDatabase.userDao(), modelMapper);
    }

    @Provides
    @Singleton
    LocalScreeningDao providesLocalScreeningDao(AppDatabase appDatabase, ModelMapper modelMapper) {
        return new LocalScreeningDaoAdapter(appDatabase.screeningDao(), appDatabase.movieDao(), modelMapper);
    }

    @Provides
    @Singleton
    MovieDaoLocal providesLocalMovieDao(AppDatabase appDatabase, ModelMapper modelMapper) {
        return new MovieDaoAdapter(appDatabase.movieDao(), modelMapper);
    }

    @Provides
    @Singleton
    ReservationDaoLocal providesReservationDaoLocal(AppDatabase appDatabase, ModelMapper modelMapper){
        return new ReservationDaoLocalAdapter(appDatabase.reservationDao(), modelMapper);
    }

    @Provides
    @Singleton
    MovieFeedbackDaoLocal providesLocalMovieFeedbackDao(AppDatabase appDatabase, ModelMapper modelMapper){
        return new MovieFeedbackDaoAdapter(appDatabase.movieFeedbackDao(), modelMapper);
    }
}
