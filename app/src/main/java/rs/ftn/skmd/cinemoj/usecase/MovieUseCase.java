package rs.ftn.skmd.cinemoj.usecase;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.MovieDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.MovieDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class MovieUseCase {

    private MovieDaoLocal movieDaoLocal;

    private MovieDaoRemote movieDaoRemote;

    private NetworkManager networkManager;

    public MovieUseCase(MovieDaoLocal movieDaoLocal, MovieDaoRemote movieDaoRemote,
                        NetworkManager networkManager) {
        this.movieDaoLocal = movieDaoLocal;
        this.movieDaoRemote = movieDaoRemote;
        this.networkManager = networkManager;
    }

    public Flowable<ResultWithData<List<Movie>>> getAllMovies() {
        if (!networkManager.isNetworkAvailable()) {
            return movieDaoLocal.getAll();
        }
        return this.movieDaoRemote.getAll()
                .doOnSuccess(this::handleGetAllMovies)
                .doOnError(this::readCachedData)
                .subscribeOn(Schedulers.io()).toFlowable();
    }

    public Completable toggleFavorite(Long movieId) {
        return Completable.fromAction(() -> {
            final Movie movie = this.movieDaoLocal.getById(movieId).blockingFirst().getValue();
            movie.setFavorite(!movie.isFavorite());
            this.movieDaoLocal.save(movie).blockingAwait();
        }).subscribeOn(Schedulers.io());

    }

    private ResultWithData<List<Movie>> handleGetAllMovies(ResultWithData<List<Movie>> result) {
        if (!result.isSuccess()) {
            return new ResultWithData<>(result.getError());
        }
        final List<Movie> movies = result.getValue();
//        for (Movie m : movies) {
//            movieDaoLocal.delete(m.getId()).blockingAwait();
//        }
        for (Movie savedMovie : movieDaoLocal.getAll().blockingFirst().getValue()) {
            for (Movie m : movies) {
                if (savedMovie != null && savedMovie.getId() != null
                        && savedMovie.getId().equals(m.getId())) {
                    m.setFavorite(savedMovie.isFavorite());
                    movieDaoLocal.delete(m.getId()).blockingAwait();
                    break;
                }
            }
        }

        movieDaoLocal.saveAll(movies).blockingAwait();
        return result;
    }

    private ResultWithData<List<Movie>> readCachedData(Throwable throwable) {
        ResultWithData<List<Movie>> movies = movieDaoLocal.getAll().blockingFirst();
        return movies;
    }

}
