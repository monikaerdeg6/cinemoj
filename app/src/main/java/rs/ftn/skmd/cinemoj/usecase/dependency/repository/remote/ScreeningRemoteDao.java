package rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote;

import java.util.List;

import io.reactivex.Single;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public interface ScreeningRemoteDao {

    Single<ResultWithData<List<Screening>>> getAll();

    Single<Result> makeReservation(Long screeningId, Integer ticketsNumber);

    Single<ResultWithData<List<MovieFeedback>>> getMovieComments(Long movie_id);
}
