package rs.ftn.skmd.cinemoj.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;

import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.ui.adapter.listener.ReservationSelectedListener;

@EViewGroup(R.layout.view_item_reservation)
public class ReservationItemView extends RelativeLayout {

    @ViewById
    TextView movieTitle;

    @ViewById
    TextView movieDateTime;

    @ViewById
    TextView hallSeats;

    @ViewById
    ImageView moviePoster;

    public ReservationItemView(Context context) {
        super(context);
    }

    public void bind(Reservation reservation, ReservationSelectedListener reservationSelectedListener) {
        String formatted = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(reservation.getDate());
        movieTitle.setText(reservation.getMovie().getName());
        String dt = formatted.split(" ")[0] + "/" + formatted.split(" ")[1];
        movieDateTime.setText(dt);
        String hs = reservation.getHall() + "/" + reservation.getSeats();
        hallSeats.setText(hs);

        loadPosterImage(reservation.getMovie().getPictureUrl());

        this.setOnClickListener(v -> {
            if (reservationSelectedListener != null) {
                reservationSelectedListener.reservationSelected(reservation);
            }
        });
    }

    @UiThread
    public void loadPosterImage(String url) {

        Glide.with(this)
                .asBitmap()
                .load(url)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        moviePoster.setImageBitmap(resource);
                    }
                });
    }
}
