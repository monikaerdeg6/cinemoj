package rs.ftn.skmd.cinemoj.usecase;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.ui.localization.LocaleManager;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalUserDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.AuthenticationRemoteDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.system.FirebaseRegistrationTokenProvider;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;
import rs.ftn.skmd.cinemoj.usecase.model.Session;

public class AuthenticationUseCase {

    private static final String TAG = AuthenticationUseCase.class.getSimpleName();

    private final AuthenticationRemoteDao authenticationRemoteDao;

    private final UserUseCase userUseCase;

    private final LocalUserDao localUserDao;

    private final FirebaseRegistrationTokenProvider firebaseTokenProvider;

    public AuthenticationUseCase(AuthenticationRemoteDao authenticationRemoteDao, UserUseCase userUseCase,
                                 LocalUserDao localUserDao, FirebaseRegistrationTokenProvider firebaseTokenProvider) {
        this.authenticationRemoteDao = authenticationRemoteDao;
        this.userUseCase = userUseCase;
        this.localUserDao = localUserDao;
        this.firebaseTokenProvider = firebaseTokenProvider;
    }

    public Single<Result> registerUser(String email, String password, String fullName) {
        return authenticationRemoteDao.register(email, password, fullName).subscribeOn(Schedulers.io());
    }

    public Single<ResultWithData<User>> login(String email, String password) {
        return authenticationRemoteDao.login(email, password)
                .map(this::handleLoginResponse)
                .subscribeOn(Schedulers.io())
                .map(this::sendFirebaseRegistrationTokenOrReturnUnsuccessfulResult);
    }

    public Single<Result> setLanguage(Language language) {
       return authenticationRemoteDao.setLanguage(language.name())
                .subscribeOn(Schedulers.io());
    }

    private ResultWithData<User> handleLoginResponse(ResultWithData<Session> sessionResult) {
        if (!sessionResult.isSuccess()) {
            return new ResultWithData<>(sessionResult.getError());
        }

        userUseCase.saveToken(sessionResult.getValue().getToken());
        User user = sessionResult.getValue().getUser();

        userUseCase.saveUserId(user.getId());
        userUseCase.setLoggedIn(true, user.getId());

        localUserDao.save(user).subscribe();
        return new ResultWithData<>(user);
    }

    private ResultWithData<User> sendFirebaseRegistrationTokenOrReturnUnsuccessfulResult(ResultWithData<User> userResult) {
        if (!userResult.isSuccess()) {
            return userResult;
        }

        String firebaseToken = firebaseTokenProvider.getToken().blockingGet();
        Result sendFirebaseTokenResult = sendFirebaseRegistrationToken(firebaseToken).blockingGet();

        if (!sendFirebaseTokenResult.isSuccess()) {
            clearCache();
            return new ResultWithData<>(sendFirebaseTokenResult.getError());
        }

        return userResult;
    }

    public Single<Result> logout() {
        String token = userUseCase.getFirebaseRegistrationTokenToken();

        return authenticationRemoteDao.deleteFirebaseRegistrationToken(token)
                .doOnSuccess(result -> {
                    if (result.isSuccess()) {
                        clearCache();
                    }
                }).subscribeOn(Schedulers.io());
    }

    public Single<Result> sendFirebaseRegistrationToken(String token) {
        return authenticationRemoteDao.sendFirebaseRegistrationToken(token)
                .doOnSuccess(result -> handleSendFirebaseRegistrationTokenResult(result, token))
                .subscribeOn(Schedulers.io());
    }

    private void handleSendFirebaseRegistrationTokenResult(Result result, String token) {
        if (result.isSuccess()) {
            userUseCase.saveFirebaseRegistrationTokenToken(token);
        }
    }

    private void clearCache() {
        userUseCase.setLoggedIn(false, -1L);
        userUseCase.clearAll();
    }

}
