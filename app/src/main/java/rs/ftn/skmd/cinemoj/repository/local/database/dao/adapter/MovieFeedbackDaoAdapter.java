package rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter;

import org.modelmapper.ModelMapper;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.MovieFeedbackDao;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieFeedbackDb;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.MovieFeedbackDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class MovieFeedbackDaoAdapter implements MovieFeedbackDaoLocal {

    private final ModelMapper modelMapper;

    private final MovieFeedbackDao movieFeedbackDao;

    public MovieFeedbackDaoAdapter(MovieFeedbackDao movieFeedbackDao, ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.movieFeedbackDao = movieFeedbackDao;
    }

    @Override
    public Completable save(MovieFeedback movieFeedback) {
        return Completable.fromAction(() -> {
            MovieFeedbackDb movieFeedbackDb = modelMapper.map(movieFeedback, MovieFeedbackDb.class);
            movieFeedbackDao.create(movieFeedbackDb);
        });
    }

    @Override
    public Single<List<MovieFeedback>> getAllForMovie(Long movieId) {

        return movieFeedbackDao.getAllForMovie(movieId).flatMapIterable(list -> list)
                .map(this::mapToMovie)
                .toList();

    }

    @Override
    public Flowable<ResultWithData<MovieFeedback>> getByReservationId(Long id) {
        return movieFeedbackDao.getByReservationId(id).map(this::mapToMovie).map(this::mapToResultById);
    }

    private MovieFeedback mapToMovie(MovieFeedbackDb movieFeedbackDb) {
        return modelMapper.map(movieFeedbackDb, MovieFeedback.class);
    }

    private ResultWithData<MovieFeedback> mapToResultById(MovieFeedback movie) {
        return new ResultWithData<>(movie);
    }
}
