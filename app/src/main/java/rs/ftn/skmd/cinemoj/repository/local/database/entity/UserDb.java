package rs.ftn.skmd.cinemoj.repository.local.database.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.UserDb.USER_TABLE_NAME;

@Entity(tableName = USER_TABLE_NAME)
public class UserDb implements Serializable {

    public static final String USER_TABLE_NAME = "user";

    @PrimaryKey(autoGenerate = true)
    private Long id;

    private String name;

    private String username;

    private int language;

    public UserDb() {
    }

    @Ignore
    public UserDb(Long id, String name, String username, int language) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getLanguage() {
        return language;
    }

    public void setLanguage(int language) {
        this.language = language;
    }
}
