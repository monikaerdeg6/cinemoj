package rs.ftn.skmd.cinemoj.repository.remote.dao;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.repository.remote.api.ApiService;
import rs.ftn.skmd.cinemoj.repository.remote.dto.MovieDto;
import rs.ftn.skmd.cinemoj.repository.remote.dto.MovieFeedbackDTO;
import rs.ftn.skmd.cinemoj.repository.remote.dto.converter.DateConverter;
import rs.ftn.skmd.cinemoj.repository.remote.error_extractor.ErrorExtractor;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.ReservationDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class ReservationRemoteDaoImpl extends BaseRemoteDaoImpl implements ReservationDaoRemote {

    private final ApiService apiService;

    private final ModelMapper modelMapper;

    public ReservationRemoteDaoImpl(ErrorExtractor errorExtractor, ApiService apiService, ModelMapper modelMapper) {
        super(errorExtractor);
        this.apiService = apiService;
        this.modelMapper = modelMapper;
    }

    @Override
    public Single<ResultWithData<Reservation>> getById(Long id) {
        return apiService.getMovieFeedback(id).map(this::convertResponseToReservation);
    }

    @Override
    public Single<ResultWithData<List<Reservation>>> getAll() {
        return apiService.getMyReservations().map(this::convertResponseToReservations);
    }

    private ResultWithData<Reservation> convertResponseToReservation(Response<MovieFeedbackDTO> response) {

        if (response == null || response.body() == null) {
            return null;
        }
        Type type = new TypeToken<Reservation>() {
        }.getType();
        Reservation reservation = modelMapper.map(response.body(), type);

        return new ResultWithData<>(reservation);
    }

    private ResultWithData<List<Reservation>> convertResponseToReservations(Response<List<MovieFeedbackDTO>> response) {
        if (response == null || response.body() == null) {
            return null;
        }
        List<Reservation> reservations = new ArrayList<>();
        Type type = new TypeToken<List<Reservation>>() {
        }.getType();
        response.body().forEach(dto -> {
            Reservation reservation = new Reservation();
            reservation.setReservation_id(dto.getReservation_id());
            reservation.setUser_id(dto.getUser_id());
            reservation.setDate(DateConverter.fromString(dto.getDate()));
            reservation.setHall(dto.getHall());
            reservation.setText(dto.getText());
            reservation.setSeats(dto.getSeats());
            reservation.setRate(dto.getRate());
            Movie movie = modelMapper.map(dto.getMovie(), Movie.class);
            reservation.setMovie(movie);
            reservations.add(reservation);
        });

        return new ResultWithData<>(reservations);
    }
}
