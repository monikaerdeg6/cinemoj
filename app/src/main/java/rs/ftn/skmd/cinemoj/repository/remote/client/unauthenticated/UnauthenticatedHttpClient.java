package rs.ftn.skmd.cinemoj.repository.remote.client.unauthenticated;


import rs.ftn.skmd.cinemoj.repository.remote.api.UnauthenticatedApiService;

public interface UnauthenticatedHttpClient {

    UnauthenticatedApiService getApiService();
}
