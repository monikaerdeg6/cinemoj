package rs.ftn.skmd.cinemoj.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.ui.adapter.CommentAdapter;
import rs.ftn.skmd.cinemoj.ui.fragment.MovieInfoFragment;
import rs.ftn.skmd.cinemoj.usecase.ScreeningUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;
import rs.ftn.skmd.cinemoj.utils.DateTimeUtils;

@EActivity(R.layout.activity_screening_details)
public class ScreeningDetailsActivity extends BaseActivity {

    private static final String TAG = ScreeningDetailsActivity.class.getSimpleName();

    @Extra
    Screening screening;

    @FragmentById
    MovieInfoFragment movieInfoFragment;

    @ViewById
    RecyclerView recyclerView;

    @ViewById
    TextView screeningDate;

    @ViewById
    TextView screeningHall;

    @Bean
    CommentAdapter adapter;

    @ViewById
    EditText numOfTickets;

    @Inject
    ScreeningUseCase screeningUseCase;

    @App
    CinemojApplication cinemojApplication;

    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);
        displayDrawer();
        movieInfoFragment.bind(screening.getMovie(), this);
        loadScreeningInfo();
        loadComments();
    }

    @Click
    void reserve(){
        compositeDisposable.add(screeningUseCase.makeReservations(screening.getId(),
                Integer.parseInt(numOfTickets.getText().toString()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleSuccess,
                        this::handleDataLoadFailed));
    }

    private void loadScreeningInfo() {
        screeningDate.setText(String.format(Locale.getDefault(), "%s: %s",
                getString(R.string.when), DateTimeUtils.format(screening.getDate())));
        screeningHall.setText(String.format(Locale.getDefault(), "%s: %s",
                getString(R.string.where), screening.getHall()));
    }

    private void loadComments() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false));
        recyclerView.setAdapter(adapter);
        compositeDisposable.add(screeningUseCase.getMovieComments(screening.getMovie().getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleSuccessComments,
                        this::handleDataLoadFailed));

    }

    private void handleDataLoadFailed(Throwable error) {
        Log.e(TAG, error.getMessage());
    }

    private void handleSuccess(Result result) {
        if(result.getError() != null){
            if(result.getError().getCode() == 400){
                showToast(getString(R.string.no_more_tickets), Toast.LENGTH_SHORT);
            }
        }
        else {
            showToast(getString(R.string.tickets_reserved), Toast.LENGTH_SHORT);
            MyReservationsActivity_.intent(this).start();
            finish();
        }
    }

    private void handleSuccessComments(ResultWithData<List<MovieFeedback>> result) {
        if(result.getError() != null){
            if(result.getError().getCode() == 400){
                showToast(getString(R.string.failed_to_load_comments), Toast.LENGTH_SHORT);
            }
        }
        else {
            adapter.setList(result.getValue());
        }
    }
}
