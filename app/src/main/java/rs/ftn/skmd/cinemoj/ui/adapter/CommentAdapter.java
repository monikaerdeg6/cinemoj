package rs.ftn.skmd.cinemoj.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.ui.adapter.generic.RecyclerViewAdapterBase;
import rs.ftn.skmd.cinemoj.ui.view.CommentItemView;
//import rs.ftn.skmd.cinemoj.ui.view.CommentItemView_;
import rs.ftn.skmd.cinemoj.ui.view.CommentItemView_;
import rs.ftn.skmd.cinemoj.ui.view.generic.ViewWrapper;

@EBean
public class CommentAdapter extends RecyclerViewAdapterBase<MovieFeedback, CommentItemView> {

    @RootContext
    Context context;

    @Override
    protected CommentItemView onCreateItemView(ViewGroup parent, int viewType) {
        return CommentItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewWrapper<CommentItemView> viewHolder, int position) {
        final CommentItemView view = viewHolder.getView();
        final MovieFeedback movieFeedback = items.get(position);
        view.bind(movieFeedback);
    }

    public void setList(List<MovieFeedback> movieFeedbacks) {
        this.items = movieFeedbacks;
        notifyDataSetChanged();
    }
}
