package rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote;

import io.reactivex.Single;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;
import rs.ftn.skmd.cinemoj.usecase.model.Session;

public interface AuthenticationRemoteDao {

    Single<Result> register(String email, String password, String fullName);

    Single<ResultWithData<Session>> login(String email, String password);

    Single<Result> sendFirebaseRegistrationToken(String firebaseRegistrationToken);

    Single<Result> deleteFirebaseRegistrationToken(String firebaseRegistrationToken);

    Single<Result> setLanguage(String language);
}
