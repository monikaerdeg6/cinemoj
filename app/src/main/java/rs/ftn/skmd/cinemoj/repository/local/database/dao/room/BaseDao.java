package rs.ftn.skmd.cinemoj.repository.local.database.dao.room;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import java.util.List;

public interface BaseDao<T>{

    @Delete
    void delete(T entity);

    @Update
    void update(T entity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long create(T entity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void createAll(List<T> entities);
}
