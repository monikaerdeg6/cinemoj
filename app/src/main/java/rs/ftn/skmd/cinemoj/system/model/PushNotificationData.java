package rs.ftn.skmd.cinemoj.system.model;

public class PushNotificationData {

    private String title;

    private String body;

    public PushNotificationData() {
    }

    public PushNotificationData(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

