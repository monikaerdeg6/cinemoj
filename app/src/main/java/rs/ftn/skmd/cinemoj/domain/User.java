package rs.ftn.skmd.cinemoj.domain;

public class User {

    private Long id;

    private String name;

    private String email;

    private Language language;

    public User() {
    }

    public User(Long id, String name, String email, Language language) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.language = language;
    }

    public User(Long id, String name, String email, String language) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.language = language.equals("SERBIAN") ? Language.SERBIAN : Language.ENGLISH;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public void setLanguage(String language) {
        this.language = language.equals("SERBIAN") ? Language.SERBIAN : Language.ENGLISH;
    }
}
