package rs.ftn.skmd.cinemoj.ui.adapter.listener;

import rs.ftn.skmd.cinemoj.domain.Movie;

public interface MovieSelectedListener {

    void movieSelected(Movie movie);
}
