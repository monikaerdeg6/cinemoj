package rs.ftn.skmd.cinemoj.ui.activity;

import android.support.design.widget.NavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.Language;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.ui.adapter.MyReservationsAdapter;
import rs.ftn.skmd.cinemoj.usecase.ReservationUseCase;
import rs.ftn.skmd.cinemoj.usecase.UserUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

@EActivity(R.layout.activity_my_reservations)
public class MyReservationsActivity extends BaseActivity {

    private static final String TAG = MyReservationsActivity.class.getSimpleName();

    @Bean
    MyReservationsAdapter adapter;

    @ViewById
    RecyclerView recyclerView;

    @App
    CinemojApplication cinemojApplication;

    @Inject
    ReservationUseCase reservationUseCase;

    @Inject
    UserUseCase userUseCase;

    @AfterViews
    void init() {
        cinemojApplication.getDiComponent().inject(this);
        displayDrawer();
        loadReservations();
        initTitle();
    }

    private void initTitle() {
        if (userUseCase.getLoggedInUser() == null) return;
        if (userUseCase.getLanguage() == Language.ENGLISH) {
            setTitle("My reservations");
        } else {
            setTitle("Moje rezervacije");
        }
    }

    private void loadReservations() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false));
        recyclerView.setAdapter(adapter);

        adapter.setReservationSelectedListener(reservation -> {
            LeaveFeedbackActivity_.intent(this).reservation(reservation).start();
        });

        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(3).setChecked(true);
    }

    private void loadData() {
        compositeDisposable.add(reservationUseCase.getAllReservations()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleDataLoaded,
                        this::handleDataLoadFailed));


    }

    private void handleDataLoaded(ResultWithData<List<Reservation>> result) {
        if (!result.isSuccess()) {
            return;
        }
        adapter.setList(result.getValue());
    }

    private void handleDataLoadFailed(Throwable error) {
        Log.e(TAG, error.getMessage());
    }

}
