package rs.ftn.skmd.cinemoj.ui.activity.base;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import javax.inject.Inject;

import de.adorsys.android.securestoragelibrary.SecurePreferences;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.ui.activity.LoginActivity_;
import rs.ftn.skmd.cinemoj.ui.activity.MyReservationsActivity_;
import rs.ftn.skmd.cinemoj.ui.activity.ScreeningsActivity_;
import rs.ftn.skmd.cinemoj.ui.activity.SettingsActivity_;
import rs.ftn.skmd.cinemoj.ui.activity.SoonActivity_;
import rs.ftn.skmd.cinemoj.ui.activity.WhereToFindUsActivity_;
import rs.ftn.skmd.cinemoj.usecase.AuthenticationUseCase;
import rs.ftn.skmd.cinemoj.usecase.UserUseCase;
import rs.ftn.skmd.cinemoj.usecase.model.Result;

@SuppressLint("Registered")
@EActivity(R.layout.activity_base)
public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    AuthenticationUseCase authenticationUseCase;

    @Inject
    UserUseCase userUseCase;

    @Override
    public void setContentView(int layoutResID) {

        final LinearLayout fullView = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        final FrameLayout activityContainer = fullView.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);

        super.setContentView(fullView);
    }

    @AfterViews
    public void displayToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void setTitle(int id) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(id);
    }

    public void displayDrawer() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setUser();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = findViewById(R.id.drawer);
        drawer.closeDrawer(GravityCompat.START);
        new Handler().postDelayed(() -> handleItemSelection(id), 300);
        return true;
    }

    void handleItemSelection(int id) {
        if (id == R.id.nav_screenings) {
            ScreeningsActivity_.intent(this)
                    .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .start();
        } else if (id == R.id.nav_soon) {
            SoonActivity_.intent(this)
                    .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .start();
        } else if (id == R.id.nav_map) {
            WhereToFindUsActivity_.intent(this)
                    .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .start();
        } else if (id == R.id.nav_reservations) {
            MyReservationsActivity_.intent(this)
                    .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .start();
        } else if (id == R.id.nav_settings) {
            SettingsActivity_.intent(this)
                    .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .start();
        } else if (id == R.id.nav_logout) {
            if (authenticationUseCase != null) {
                compositeDisposable.add(authenticationUseCase.logout()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::handleLogoutSuccess, this::onError));
                return;
            }
        }
        finish();
    }
    private void handleLogoutSuccess(Result result) {
        ScreeningsActivity_.intent(this)
                .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .start();
    }

    public void showToast(String text, int length) {
        Toast.makeText(this, text, length).show();
    }

    protected void onError(Throwable throwable) {
        String message = throwable.getMessage();
        String errorMessage = getString(R.string.unknown_error);

        if (message != null) {
            errorMessage = message;
        }

        showToast(errorMessage, Toast.LENGTH_SHORT);
    }

    public void setUser() {
        User user = userUseCase.getLoggedInUser();
        if (user == null) {
            return;
        }
        NavigationView navigationView = findViewById(R.id.nav_view);
        TextView fullNameView = navigationView.getHeaderView(0).findViewById(R.id.userFullName);
        TextView emailView = navigationView.getHeaderView(0).findViewById(R.id.userEmail);
        if (fullNameView != null) {
            fullNameView.setText(user.getName());
            emailView.setText(user.getEmail());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
