package rs.ftn.skmd.cinemoj.repository.remote.dto;

import java.io.Serializable;
import java.util.List;

public class MovieDto implements Serializable {

    private Long id;

    private String name;

    private String description;

    private int duration;

    private int year;

    private int rating;

    private String genres;

    private String pictureUrl;

    private List<MovieFeedbackDTO> comments;

    public MovieDto() {
    }

    public MovieDto(Long id, String name, String description, int duration, int year, int rating,
                    String genres, String pictureUrl, List<MovieFeedbackDTO> comments) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.duration = duration;
        this.year = year;
        this.rating = rating;
        this.genres = genres;
        this.pictureUrl = pictureUrl;
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public List<MovieFeedbackDTO> getComments() {
        return comments;
    }

    public void setComments(List<MovieFeedbackDTO> comments) {
        this.comments = comments;
    }
}
