package rs.ftn.skmd.cinemoj.repository.remote.dto;

public class SoonMovieDetailsDTO {

    private Long id;
    private String name;
    private String description;
    private Integer duration;
    private Integer year;
    private Integer rating;
    private String genres;
    private String pictureUrl;

    public SoonMovieDetailsDTO() {
    }

    public SoonMovieDetailsDTO(Long id, String name, String description, Integer duration, Integer year, Integer rating, String genres, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.duration = duration;
        this.year = year;
        this.rating = rating;
        this.genres = genres;
        this.pictureUrl = pictureUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
