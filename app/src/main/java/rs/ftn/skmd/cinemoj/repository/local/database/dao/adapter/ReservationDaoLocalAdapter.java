package rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter;

import org.modelmapper.ModelMapper;
import org.reactivestreams.Publisher;

import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.ReservationDao;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieDb;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.ReservationDb;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.ReservationDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class ReservationDaoLocalAdapter implements ReservationDaoLocal {

    private final ModelMapper modelMapper;

    private final ReservationDao reservationDao;

    public ReservationDaoLocalAdapter(ReservationDao reservationDao, ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.reservationDao = reservationDao;
    }

    @Override
    public Completable save(Reservation reservation) {
        return Completable.fromAction(() -> {
            ReservationDb reservationDb = new ReservationDb();
            reservationDb.setReservation_id(reservation.getReservation_id());
            reservationDb.setUser_id(reservation.getUser_id());
            reservationDb.setDate(reservation.getDate());
            reservationDb.setHall(reservation.getHall());
            reservationDb.setText(reservation.getText());
            reservationDb.setSeats(reservation.getSeats());
            reservationDb.setRate(reservation.getRate());
            MovieDb movie = modelMapper.map(reservation.getMovie(), MovieDb.class);
            reservationDb.setMovie(movie);
            reservationDao.create(reservationDb);
        });
    }

    @Override
    public Flowable<ResultWithData<Reservation>> getById(Long id) {
        return reservationDao.getById(id).map(this::mapToReservation).map(this::mapToResultById);
    }

    @Override
    public Flowable<ResultWithData<List<Reservation>>> getAll() {
        return reservationDao.getAll().concatMap((Function<List<ReservationDb>,
                Publisher<ResultWithData<List<Reservation>>>>) galleryWithImages ->
                Flowable.fromIterable(galleryWithImages)
                        .map(this::mapToReservation)
                        .toList()
                        .map(this::mapToResponse)
                        .toFlowable());
    }

    private ResultWithData<List<Reservation>> mapToResponse(List<Reservation> reservations) {
        return new ResultWithData<>(reservations);
    }

    private Reservation mapToReservation(ReservationDb reservationDb) {
        Reservation reservation = new Reservation();
        reservation.setReservation_id(reservationDb.getReservation_id());
        reservation.setUser_id(reservationDb.getUser_id());
        reservation.setDate(reservationDb.getDate());
        reservation.setHall(reservationDb.getHall());
        reservation.setText(reservationDb.getText());
        reservation.setSeats(reservationDb.getSeats());
        reservation.setRate(reservationDb.getRate());
        Movie movie = modelMapper.map(reservationDb.getMovie(), Movie.class);
        reservation.setMovie(movie);
        return reservation;
    }

    private ResultWithData<Reservation> mapToResultById(Reservation reservation) {
        return new ResultWithData<>(reservation);
    }

    @Override
    public Completable deleteAll() {
        return Completable.fromAction(() -> reservationDao.deleteAll());
    }

    @Override
    public Completable saveAll(List<Reservation> reservations) {

        return Completable.fromAction(() -> {
            List<ReservationDb> reservationDbs = reservations.stream().map(reservation ->
                    new ReservationDb(reservation.getReservation_id(), reservation.getUser_id(),
                            reservation.getRate(), reservation.getDate().toString(), reservation.getHall(),
                            reservation.getDate(), reservation.getSeats(), new MovieDb(
                                    reservation.getMovie().getId(), reservation.getMovie().getName(),
                            reservation.getMovie().getDescription(), reservation.getMovie().getDuration(),
                            reservation.getMovie().getYear(), reservation.getMovie().getRating(),
                            reservation.getMovie().isFavorite(), reservation.getMovie().getGenres(),
                            reservation.getMovie().getPictureUrl()))).collect(Collectors.toList());
            reservationDao.createAll(reservationDbs);
        });
    }

}
