package rs.ftn.skmd.cinemoj.usecase;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import rs.ftn.skmd.cinemoj.domain.MovieFeedback;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.MovieFeedbackDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.PreferenceStorage;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.MovieFeedbackDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.usecase.model.Result;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class MovieFeedbackUseCase {

    private MovieFeedbackDaoLocal movieFeedbackDaoLocal;

    private MovieFeedbackDaoRemote movieFeedbackDaoRemote;

    private PreferenceStorage preferenceStorage;

    private final NetworkManager networkManager;

    public MovieFeedbackUseCase(MovieFeedbackDaoLocal movieFeedbackDaoLocal,
                                MovieFeedbackDaoRemote movieFeedbackDaoRemote,
                                NetworkManager networkManager,
                                PreferenceStorage preferenceStorage) {
        this.movieFeedbackDaoLocal = movieFeedbackDaoLocal;
        this.movieFeedbackDaoRemote = movieFeedbackDaoRemote;
        this.preferenceStorage = preferenceStorage;
        this.networkManager = networkManager;
    }

    public Single<Result> leaveFeedback(Long reservationId, String text, Integer rate) {

        final MovieFeedback movieFeedback = new MovieFeedback();
        movieFeedback.setReservationId(reservationId);
        movieFeedback.setText(text);
        movieFeedback.setRate(rate);
        movieFeedback.setUserId(preferenceStorage.getLoggedInUserId());
        return this.movieFeedbackDaoRemote.save(movieFeedback).subscribeOn(Schedulers.io());
    }

    public Flowable<ResultWithData<MovieFeedback>> getMovieFeedbackByReservationId(Long reservationId) {
        if (!networkManager.isNetworkAvailable()) {
            return movieFeedbackDaoLocal.getByReservationId(reservationId);
        }

        return this.movieFeedbackDaoRemote.getFeedbackForReservation(reservationId)
                .doOnSuccess(result -> handleGetAllMovies(result, reservationId))
                .doOnError(error -> readCachedData(reservationId))
                .subscribeOn(Schedulers.io()).toFlowable();
    }

    private ResultWithData<MovieFeedback> handleGetAllMovies(ResultWithData<MovieFeedback> result,
                                                             Long reservationId) {
        if (result.isSuccess()) {
            movieFeedbackDaoLocal.save(result.getValue());
            return result;
        }

        return movieFeedbackDaoLocal.getByReservationId(reservationId).blockingFirst();
    }

    private ResultWithData<MovieFeedback> readCachedData(Long reservationId) {
        final ResultWithData<MovieFeedback> movieFeedback =
                movieFeedbackDaoLocal.getByReservationId(reservationId).blockingFirst();
        return movieFeedback;
    }

//    public Flowable<ResultWithData<List<MovieFeedback>>> getAllComments(Long movieId) {
//        return this.movieFeedbackDaoRemote.getAllForMovie(movieId)
//                .doOnSuccess(result -> handleGetAllComments(result, movieId))
//                .doOnError(error -> this.readCachedData(error, movieId))
//                .subscribeOn(Schedulers.io()).toFlowable();
//    }
//

}
