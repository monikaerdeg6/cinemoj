package rs.ftn.skmd.cinemoj.usecase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalScreeningDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalUserDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.MovieDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.MovieFeedbackDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.PreferenceStorage;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.ReservationDaoLocal;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.SecureStorage;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.AuthenticationRemoteDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.MovieDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.MovieFeedbackDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.ReservationDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.ScreeningRemoteDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.system.FirebaseRegistrationTokenProvider;

@Module
public class UseCaseModule {

    @Provides
    @Singleton
    AuthenticationUseCase providesAuthenticationUseCase(AuthenticationRemoteDao authenticationRemoteDao,
                                                        UserUseCase userUseCase, LocalUserDao localUserDao,
                                                        FirebaseRegistrationTokenProvider firebaseTokenProvider) {
        return new AuthenticationUseCase(authenticationRemoteDao, userUseCase, localUserDao, firebaseTokenProvider);
    }

    @Provides
    @Singleton
    UserUseCase providesUseCase(SecureStorage secureStorage, PreferenceStorage preferenceStorage,
                                LocalUserDao localUserDao) {
        return new UserUseCase(secureStorage, preferenceStorage, localUserDao);
    }

    @Provides
    @Singleton
    ScreeningUseCase providesScreeningUseCase(ScreeningRemoteDao screeningRemoteDao,
                                              LocalScreeningDao localScreeningDao,
                                              NetworkManager networkManager) {
        return new ScreeningUseCase(screeningRemoteDao, localScreeningDao, networkManager);
    }

    @Provides
    @Singleton
    MovieUseCase providesMovieUseCase(MovieDaoLocal movieDaoLocal, MovieDaoRemote movieDaoRemote,
                                      NetworkManager networkManager) {
        return new MovieUseCase(movieDaoLocal, movieDaoRemote, networkManager);
    }

    @Provides
    @Singleton
    ReservationUseCase providesReservationUserCase(ReservationDaoLocal reservationDaoLocal,
                                                   ReservationDaoRemote reservationDaoRemote,
                                                   NetworkManager networkManager) {
        return new ReservationUseCase(reservationDaoLocal, reservationDaoRemote, networkManager);
    }

    @Provides
    @Singleton
    MovieFeedbackUseCase providesMovieFeedbackUseCase(MovieFeedbackDaoLocal movieFeedbackDaoLocal,
                                                      MovieFeedbackDaoRemote movieFeedbackDaoRemote,
                                                      PreferenceStorage preferenceStorage, NetworkManager networkManager) {
        return new MovieFeedbackUseCase(movieFeedbackDaoLocal, movieFeedbackDaoRemote, networkManager,
                preferenceStorage);
    }
}
