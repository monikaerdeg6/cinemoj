package rs.ftn.skmd.cinemoj.domain;

import java.io.Serializable;
import java.util.Date;

public class Screening implements Serializable {

    private Long id;

    private Date date;

    private Movie movie;

    private String hall;

    public Screening() {
    }

    public Screening(Long id, Date date, Movie movie, String hall) {
        this.id = id;
        this.date = date;
        this.movie = movie;
        this.hall = hall;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }
}
