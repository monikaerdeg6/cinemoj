package rs.ftn.skmd.cinemoj.system;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import rs.ftn.skmd.cinemoj.CinemojApplication;
import rs.ftn.skmd.cinemoj.R;
import rs.ftn.skmd.cinemoj.system.model.PushNotificationData;
import rs.ftn.skmd.cinemoj.ui.activity.MyReservationsActivity_;
import rs.ftn.skmd.cinemoj.usecase.AuthenticationUseCase;
import rs.ftn.skmd.cinemoj.usecase.UserUseCase;


public class FirebaseNotificationService extends FirebaseMessagingService {

    private static final String TAG = FirebaseNotificationService.class.getSimpleName();
    private static final String CHANNEL_ID = "push_notifications";

    @Inject
    NotificationChannelsManager notificationChannelsManager;

    @Inject
    AuthenticationUseCase authenticationUseCase;

    @Inject
    UserUseCase userUseCase;

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();

        ((CinemojApplication) getApplicationContext()).getDiComponent().inject(this);
        this.gson = new Gson();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        PushNotificationData notificationData = new PushNotificationData(remoteMessage.getData().get("title"),
                remoteMessage.getData().get("body"));
        if (userUseCase.isNotificationsEnabled()) {
            showNotification(notificationData, remoteMessage.getNotification());
        }
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);

        if (!userUseCase.isLoggedIn()) {
            return;
        }
        compositeDisposable.add(authenticationUseCase.sendFirebaseRegistrationToken(token).subscribe());
    }

    private void showNotification(PushNotificationData notificationData, RemoteMessage.Notification notification) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = getString(R.string.channel_name_reservation);
            notificationChannelsManager.createChannel(CHANNEL_ID, channelName);
        }

        Intent intent = MyReservationsActivity_.intent(this).get();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                R.drawable.ic_cinemoj_icon);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_cinemoj_icon)
                .setContentTitle(notificationData.getTitle())
                .setContentText(notificationData.getBody())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setLargeIcon(icon)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationData.getBody()))
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationData.hashCode(), builder.build());
    }
}
