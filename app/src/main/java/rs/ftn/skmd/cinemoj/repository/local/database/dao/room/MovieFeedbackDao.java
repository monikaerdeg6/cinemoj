package rs.ftn.skmd.cinemoj.repository.local.database.dao.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieFeedbackDb;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieFeedbackDb.MOVIE_FEEDBACK_TABLE_NAME;

@Dao
public interface MovieFeedbackDao extends BaseDao<MovieFeedbackDb> {

    @Query("SELECT * FROM " + MOVIE_FEEDBACK_TABLE_NAME + " WHERE movie_id = :movieId")
    Flowable<List<MovieFeedbackDb>> getAllForMovie(Long movieId);

    @Query("SELECT * FROM " + MOVIE_FEEDBACK_TABLE_NAME + " WHERE reservation_id = :id")
    Flowable<MovieFeedbackDb> getByReservationId(Long id);
}
