package rs.ftn.skmd.cinemoj.repository;

import org.modelmapper.ModelMapper;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    public ModelMapper providesModelMapper() {
        return new ModelMapper();
    }
}
