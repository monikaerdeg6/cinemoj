package rs.ftn.skmd.cinemoj.repository.local.database.dao.adapter;

import org.modelmapper.ModelMapper;
import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.MovieDao;
import rs.ftn.skmd.cinemoj.repository.local.database.dao.room.ScreeningDao;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.MovieDb;
import rs.ftn.skmd.cinemoj.repository.local.database.entity.ScreeningDb;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.LocalScreeningDao;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public class LocalScreeningDaoAdapter implements LocalScreeningDao {

    private final ScreeningDao screeningDao;
    private final MovieDao movieDao;
    private final ModelMapper modelMapper;

    public LocalScreeningDaoAdapter(ScreeningDao screeningDao, MovieDao movieDao, ModelMapper modelMapper) {
        this.screeningDao = screeningDao;
        this.movieDao = movieDao;
        this.modelMapper = modelMapper;
    }

    @Override
    public Completable save(Screening screening) {
        return Completable.fromAction(() -> {
            ScreeningDb screeningDb = new ScreeningDb(screening.getDate(), screening.getMovie().getId(),
                    screening.getHall(), screening.getMovie().getName());
            screeningDao.create(screeningDb);
        });
    }

    @Override
    public Flowable<Screening> getById(long id) {
        return screeningDao.getById(id).map(this::mapToScreening);
    }

    @Override
    public Flowable<ResultWithData<List<Screening>>> getAll() {
        return screeningDao.getAll().concatMap((Function<List<ScreeningDb>,
                Publisher<ResultWithData<List<Screening>>>>) galleryWithImages ->
                Flowable.fromIterable(galleryWithImages)
                        .map(this::mapToScreening)
                        .toList()
                        .map(this::mapToResult)
                        .toFlowable());
    }

    private ResultWithData<List<Screening>> mapToResult(List<Screening> screenings) {
        return new ResultWithData<>(screenings);
    }

    private Movie mapToMovie(MovieDb movieDb) {
        return modelMapper.map(movieDb, Movie.class);
    }

    private Screening mapToScreening(ScreeningDb screeningDb) {
        Screening screening = modelMapper.map(screeningDb, Screening.class);
        screening.setMovie(mapToMovie(movieDao.getById(screeningDb.getMovieId()).blockingFirst()));
        return screening;
    }

    @Override
    public Completable delete(Screening screening) {
        return Completable.fromAction(() -> screeningDao.deleteById(screening.getId()));
    }

    @Override
    public Completable deleteAll() {
        return Completable.fromAction(() -> screeningDao.deleteAll());
    }

    @Override
    public Completable saveAll(List<Screening> screenings) {

        List<MovieDb> movieDbs = screenings.stream().map(screening ->
                new MovieDb(screening.getMovie().getId(), screening.getMovie().getName(),
                        screening.getMovie().getDescription(), screening.getMovie().getDuration(),
                        screening.getMovie().getYear(), screening.getMovie().getRating(),
                        screening.getMovie().isFavorite(), screening.getMovie().getGenres(),
                        screening.getMovie().getPictureUrl())).collect(Collectors.toList());
        this.movieDao.createAll(movieDbs);

        return Completable.fromAction(() -> {
            List<ScreeningDb> screeningDbs = screenings.stream().map(screening ->
                    new ScreeningDb(screening.getDate(), screening.getMovie().getId(),
                    screening.getHall(), screening.getMovie().getName())).collect(Collectors.toList());
            screeningDao.createAll(screeningDbs);
        });
    }

}
