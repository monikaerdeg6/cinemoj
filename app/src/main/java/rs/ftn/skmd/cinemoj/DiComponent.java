package rs.ftn.skmd.cinemoj;

import javax.inject.Singleton;

import dagger.Component;
import rs.ftn.skmd.cinemoj.repository.RepositoryModule;
import rs.ftn.skmd.cinemoj.repository.local.LocalRepositoryModule;
import rs.ftn.skmd.cinemoj.repository.remote.RemoteRepositoryModule;
import rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor.HttpInterceptorImpl;
import rs.ftn.skmd.cinemoj.system.FirebaseNotificationService;
import rs.ftn.skmd.cinemoj.system.SystemModule;
import rs.ftn.skmd.cinemoj.ui.activity.LeaveFeedbackActivity;
import rs.ftn.skmd.cinemoj.ui.activity.LoginActivity;
import rs.ftn.skmd.cinemoj.ui.activity.MyReservationsActivity;
import rs.ftn.skmd.cinemoj.ui.activity.MovieDetailsActivity;
import rs.ftn.skmd.cinemoj.ui.activity.RegisterActivity;
import rs.ftn.skmd.cinemoj.ui.activity.SettingsActivity;
import rs.ftn.skmd.cinemoj.ui.activity.ScreeningDetailsActivity;
import rs.ftn.skmd.cinemoj.ui.activity.ScreeningsActivity;
import rs.ftn.skmd.cinemoj.ui.activity.SoonActivity;
import rs.ftn.skmd.cinemoj.ui.activity.WhereToFindUsActivity;
import rs.ftn.skmd.cinemoj.ui.activity.base.BaseActivity;
import rs.ftn.skmd.cinemoj.usecase.UseCaseModule;

@Component(modules = {UseCaseModule.class, RepositoryModule.class, RemoteRepositoryModule.class,
        LocalRepositoryModule.class, SystemModule.class})
@Singleton
public interface DiComponent {

    void inject(CinemojApplication application);

    void inject(BaseActivity activity);

    void inject(HttpInterceptorImpl httpInterceptor);

    void inject(LoginActivity activity);

    void inject(SettingsActivity activity);

    void inject(RegisterActivity activity);

    void inject(SoonActivity soonActivity);

    void inject(MovieDetailsActivity movieDetailsActivity);

    void inject(MyReservationsActivity myReservationsActivity);

    void inject(FirebaseNotificationService service);

    void inject(ScreeningsActivity activity);

    void inject(LeaveFeedbackActivity activity);

    void inject(ScreeningDetailsActivity activity);

    void inject(WhereToFindUsActivity activity);
}