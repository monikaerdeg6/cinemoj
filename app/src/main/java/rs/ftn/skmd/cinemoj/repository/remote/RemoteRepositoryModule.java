package rs.ftn.skmd.cinemoj.repository.remote;

import android.content.Context;

import org.modelmapper.ModelMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rs.ftn.skmd.cinemoj.repository.remote.api.ApiService;
import rs.ftn.skmd.cinemoj.repository.remote.api.UnauthenticatedApiService;
import rs.ftn.skmd.cinemoj.repository.remote.dao.MovieFeedbackRemoteDaoImpl;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.MovieFeedbackDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.NetworkManager;
import rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.AuthenticatedHttpClient;
import rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.AuthenticatedHttpClientImpl;
import rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor.HttpInterceptor;
import rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor.HttpInterceptorImpl;
import rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor.TokenReader;
import rs.ftn.skmd.cinemoj.repository.remote.client.authenticated.interceptor.TokenReaderImpl;
import rs.ftn.skmd.cinemoj.repository.remote.client.unauthenticated.UnauthenticatedHttpClient;
import rs.ftn.skmd.cinemoj.repository.remote.client.unauthenticated.UnauthenticatedHttpClientImpl;
import rs.ftn.skmd.cinemoj.repository.remote.dao.AuthenticationRemoteDaoImpl;
import rs.ftn.skmd.cinemoj.repository.remote.dao.ScreeningRemoteDaoImpl;
import rs.ftn.skmd.cinemoj.repository.remote.dao.MovieRemoteDaoImpl;
import rs.ftn.skmd.cinemoj.repository.remote.dao.ReservationRemoteDaoImpl;
import rs.ftn.skmd.cinemoj.repository.remote.error_extractor.ErrorExtractor;
import rs.ftn.skmd.cinemoj.repository.remote.error_extractor.ErrorExtractorImpl;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.local.SecureStorage;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.AuthenticationRemoteDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.ScreeningRemoteDao;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.MovieDaoRemote;
import rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote.ReservationDaoRemote;

@Module
public class RemoteRepositoryModule {

    private final Context context;

    public RemoteRepositoryModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    ErrorExtractor provideErrorExtractor() {
        return new ErrorExtractorImpl();
    }

    @Provides
    @Singleton
    UnauthenticatedHttpClient providesUnauthenticatedHttpClient(NetworkManager networkManager) {
        return new UnauthenticatedHttpClientImpl(networkManager);
    }

    @Provides
    @Singleton
    AuthenticatedHttpClient providesAuthenticatedHttpClient(HttpInterceptor httpInterceptor) {
        return new AuthenticatedHttpClientImpl(httpInterceptor);
    }

    @Provides
    @Singleton
    AuthenticationRemoteDao providesAuthenticationRemoteDao(UnauthenticatedApiService unauthenticatedApiService, ApiService apiService, ModelMapper modelMapper, ErrorExtractor errorExtractor) {
        return new AuthenticationRemoteDaoImpl(unauthenticatedApiService, apiService, modelMapper, errorExtractor);
    }

    @Provides
    @Singleton
    ScreeningRemoteDao providesScreeningRemoteDao(ApiService apiService,
                                                  ErrorExtractor errorExtractor,
                                                  ModelMapper modelMapper) {
        return new ScreeningRemoteDaoImpl(apiService, errorExtractor, modelMapper);
    }

    @Provides
    @Singleton
    UnauthenticatedApiService providesAuthenticationApiService(UnauthenticatedHttpClient unauthenticatedHttpClient) {
        return unauthenticatedHttpClient.getApiService();
    }

    @Provides
    @Singleton
    ApiService providesApiService(AuthenticatedHttpClient client) {
        return client.getApiService();
    }

    @Provides
    @Singleton
    TokenReader providesTokenReader(SecureStorage secureStorage) {
        return new TokenReaderImpl(secureStorage);
    }

    @Provides
    @Singleton
    HttpInterceptor providesHttpInterceptor(NetworkManager networkManager, TokenReader tokenReader) {
        return new HttpInterceptorImpl(networkManager, tokenReader);
    }

    @Provides
    @Singleton
    MovieDaoRemote providesMovieRemoteDao(ApiService apiService, ModelMapper modelMapper,
                                          ErrorExtractor errorExtractor) {
        return new MovieRemoteDaoImpl(apiService, modelMapper, errorExtractor);
    }

    @Provides
    @Singleton
    ReservationDaoRemote providesReservationDaoRemote(ApiService apiService, ModelMapper modelMapper,
                                                      ErrorExtractor errorExtractor){
        return new ReservationRemoteDaoImpl(errorExtractor, apiService, modelMapper);
    }

    @Provides
    @Singleton
    MovieFeedbackDaoRemote providesMovieFeedbackRemoteDao(ApiService apiService, ModelMapper modelMapper,
                                                  ErrorExtractor errorExtractor) {
        return new MovieFeedbackRemoteDaoImpl(apiService, modelMapper, errorExtractor);
    }

}
