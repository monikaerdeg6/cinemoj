package rs.ftn.skmd.cinemoj.usecase.dependency.repository.remote;

import java.util.List;

import io.reactivex.Single;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.usecase.model.ResultWithData;

public interface ReservationDaoRemote {

    Single<ResultWithData<Reservation>> getById(Long id);

    Single<ResultWithData<List<Reservation>>> getAll();
}
