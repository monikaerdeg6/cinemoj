package rs.ftn.skmd.cinemoj.repository.local.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

import static rs.ftn.skmd.cinemoj.repository.local.database.entity.ScreeningDb.SCREENING_TABLE_NAME;

@Entity(tableName = SCREENING_TABLE_NAME)
public class ScreeningDb implements Serializable {

    public static final String SCREENING_TABLE_NAME = "screening";

    static final String MOVIE_ID = "movieId";

    @PrimaryKey(autoGenerate = true)
    private Long id;

    private Date date;

    @ColumnInfo(name = MOVIE_ID)
    private long movieId;

    private String hall;

    private String movieName;

    public ScreeningDb() {
    }

    @Ignore
    public ScreeningDb(Date date, long movieId, String hall, String movieName) {
        this.date = date;
        this.movieId = movieId;
        this.hall = hall;
        this.movieName = movieName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }
}
