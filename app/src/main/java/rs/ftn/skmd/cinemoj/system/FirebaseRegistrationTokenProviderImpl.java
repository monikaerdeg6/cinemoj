package rs.ftn.skmd.cinemoj.system;

import com.google.firebase.iid.FirebaseInstanceId;

import io.reactivex.Single;
import rs.ftn.skmd.cinemoj.usecase.dependency.system.FirebaseRegistrationTokenProvider;

public class FirebaseRegistrationTokenProviderImpl implements FirebaseRegistrationTokenProvider {

    @Override
    public Single<String> getToken() {
        return Single.create(emitter -> FirebaseInstanceId.getInstance()
                .getInstanceId()
                .addOnSuccessListener(instanceIdResult -> emitter.onSuccess(instanceIdResult.getToken())));
    }
}
